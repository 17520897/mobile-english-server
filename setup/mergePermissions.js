const fs = require("fs");

function main() {
  try {
    const files = ["phong", "duy"];
    const test = require("./devPermissions/phong.json");
    let data = [];
    for (let i = 0; i < files.length; i++) {
      const permissionData = require(`./devPermissions/${files[i]}.json`);
      if (permissionData) {
        data = [...data, ...permissionData.data];
      }
    }
    console.log(data);
    fs.writeFileSync(
      "setup/permission.json",
      JSON.stringify({
        data: [...data],
      }),
      {
        encoding: "utf-8",
      }
    );
    console.log(`merge permission success`);
  } catch (error) {
    console.log(`merge permission file error: `, error);
    process.exit(1);
  }
}

function employeePermission() {
  try {
    const files = ["phong-employeePermissions", "duy-employeePermissions"];
    let data = [];
    for (let i = 0; i < files.length; i++) {
      const permissionData = require(`./devPermissions/${files[i]}.json`);
      if (permissionData) {
        data = [...data, ...permissionData.data];
      }
    }
    console.log(data);
    fs.writeFileSync(
      "setup/employeePermission.json",
      JSON.stringify({
        data: [...data],
      }),
      {
        encoding: "utf-8",
      }
    );
    console.log(`merge permission success`);
  } catch (error) {
    console.log(`merge permission file error: `, error);
    process.exit(1);
  }
}

main();
employeePermission();
