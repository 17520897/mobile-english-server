const fs = require("fs");
const data = require("./newLocal.json");
function sortByName(payload, des) {
  return payload.sort((a, b) => {
    let aSplit = a.name.split(" ");
    let bSplit = b.name.split(" ");
    if (
      aSplit[1] &&
      bSplit[1] &&
      Number.isInteger(parseInt(aSplit[1])) &&
      Number.isInteger(parseInt(bSplit[1]))
    ) {
      return parseInt(aSplit[1]) < parseInt(bSplit[1]) ? -1 : 1;
    } else if (aSplit[1] && Number.isInteger(parseInt(aSplit[1]))) {
      return -1;
    } else if (bSplit[1] && Number.isInteger(parseInt(bSplit[1]))) {
      return 1;
    } else {
      if (
        aSplit[0] === "Quận" &&
        (bSplit[0] === "Huyện" || bSplit[0] === "Thị")
      ) {
        return -1;
      } else if (
        bSplit[0] === "Quận" &&
        (aSplit[0] === "Huyện" || aSplit[0] === "Thị")
      ) {
        return 1;
      } else if (aSplit[0] === "Thị" && bSplit[0] === "Huyện") {
        return -1;
      } else if (bSplit[0] === "Thị" && aSplit[0] === "Huyện") {
        return 1;
      } else if (aSplit[0] === bSplit[0]) {
        return aSplit[1] > bSplit[1] ? 1 : -1;
      } else {
        a.name > b.name ? -1 : 1;
      }
    }
  });
}

function sortWards(payload) {
  return payload.sort((a, b) => {
    let aSplit = a.name.split(" ");
    let bSplit = b.name.split(" ");
    if (
      aSplit[1] &&
      bSplit[1] &&
      Number.isInteger(parseInt(aSplit[1])) &&
      Number.isInteger(parseInt(bSplit[1]))
    ) {
      return parseInt(aSplit[1]) < parseInt(bSplit[1]) ? -1 : 1;
    } else if (aSplit[1] && Number.isInteger(parseInt(aSplit[1]))) {
      return -1;
    } else if (bSplit[1] && Number.isInteger(parseInt(bSplit[1]))) {
      return 1;
    } else {
      if (aSplit[0] === "Phường" && bSplit[0] === "Xã") {
        return -1;
      } else if (bSplit[0] === "Phường" && aSplit[0] === "Xã") {
        return 1;
      } else if (aSplit[0] === bSplit[0]) {
        return aSplit[1] > bSplit[1] ? 1 : -1;
      } else {
        a.name > b.name ? -1 : 1;
      }
    }
  });
}
function main() {
  data.sort((a, b) => {
    return a.name > b.name ? 1 : -1;
  });
  for (let i = 0; i < data.length; i++) {
    sortByName(data[i].districts);
    for (let j = 0; j < data[i].districts.length; j++) {
      sortWards(data[i].districts[j].wards);
    }
  }
  fs.writeFileSync("./newLcoal2.json", JSON.stringify(data));
}

main();
