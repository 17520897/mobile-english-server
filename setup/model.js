const mongoose = require("mongoose");

const adminModel = new mongoose.Schema(
  {
    username: {
      type: String,
      trim: true,
      unique: true,
    },
    name: {
      type: String,
    },
    phone: {
      type: String,
      trim: true,
    },
    password: {
      type: String,
    },
  },
  {
    collection: "Admins",
  }
);

mongoose.model("Admins", adminModel);
