const mongoose = require("mongoose");
const fs = require("fs");
const bcrypt = require("bcrypt");
require("./model");
require("dotenv").config();

async function connectMongoose() {
  if (process.argv.length <= 2) {
    console.log("must have params: [dev, staging, production]");
    return;
  }
  try {
    const env = process.argv[2];
    console.log(env);
    let dbString = process.env.MONGODB_URI_DEV;
    if (env.toString() === "production") {
      dbString = process.env.MONGODB_URI;
    } else if (env.toString() === "staging") {
      dbString = process.env.MONGODB_URI_STAGING;
    }
    console.log(`Begin connect to: ${dbString}`);
    mongoose.connect(
      dbString,
      {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
      () => {
        console.log("Connect to mongoose: ", dbString);
      }
    );
  } catch (error) {
    console.log("connect mongoose fail: ", error);
  }
}

function setupUploadFolder() {
  try {
    const checkPublic = fs.existsSync("./public");
    if (!checkPublic) {
      console.log("[Setup Uploads Folder] Create public upload folder");
      fs.mkdirSync("./public");
    }
    const checkUploads = fs.existsSync("./public/uploads");
    if (!checkUploads) {
      console.log("[Setup Uploads Folder] Create uploads upload folder");
      fs.mkdirSync("./public/uploads");
    }
    console.log("[Setup Uploads Folder] Setup upload folder success");
  } catch (error) {
    console.log("[Setup Uploads Folder] Setup upload folder error: ", error);
    process.exit(1);
  }
}

async function setupAdminAccount() {
  try {
    const { username, phone, name } = {
      username: "admin",
      phone: "0327888662",
      name: "System Admin",
    };
    const checkAdmins = await mongoose.model("Admins").findOne({
      username,
    });
    if (!checkAdmins) {
      const salt = await bcrypt.genSalt(10);
      const hash = await bcrypt.hash(username, salt);
      await mongoose.model("Admins").create({
        username,
        password: hash,
        name,
        phone,
      });
    }
    console.log("[Setup Admins] Setup system admin account success");
  } catch (error) {
    console.log("[Setup Admins] Setup system admin error: ", error);
    process.exit(1);
  }
}
async function main() {
  await connectMongoose();
  await setupAdminAccount();
  setupUploadFolder();
  process.exit();
}

main();
