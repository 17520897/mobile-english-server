import log4js from "log4js";

const log4j = (path) => {
  return log4js.getLogger(path);
};

export default log4j;
