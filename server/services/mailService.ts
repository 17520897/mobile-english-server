import nodemailer from "nodemailer";
import log4j from "../common/log4js";
import { ErrorsCode } from "../const/errorsCode";

const log = log4j("MailService");

interface ISENDMAIL {
  to: string;
  subject: string;
  text?: string;
  html?: string;
}
export class SendMail {
  async send(iSendMail: ISENDMAIL) {
    try {
      const { to, subject, text, html } = iSendMail;
      const transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user:
            process.env.NODE_ENV === "dev"
              ? process.env.DEV_EMAIL_USERNAME
              : process.env.EMAIL_USERNAME,
          pass:
            process.env.NODE_ENV === "dev"
              ? process.env.DEV_EMAIL_PASSWORD
              : process.env.EMAIL_PASSWORD,
        },
      });
      console.log(transporter.transporter);
      let response = await transporter.sendMail({
        from: `Pawn Codosa <${
          process.env.NODE_ENV === "dev"
            ? process.env.DEV_EMAIL_USERNAME
            : process.env.EMAIL_USERNAME
        }>`,
        to,
        subject,
        text,
        html,
      });

      return {
        message: "send mail success",
        data: response,
      };
    } catch (error) {
      log.error(`Mail: ${error}`);
      return {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      };
    }
  }
}

export default new SendMail();
