import axios, { AxiosRequestConfig } from "axios";
import l from "../common/logger";
import log4j from "../common/log4js";
const log = log4j("AxiosRequestService");
export class AxiosRequestService {
  async get(url: string, options: AxiosRequestConfig = {}) {
    try {
      const data = await axios.get(url, options);
      return data;
    } catch (error) {
      l.error(`axiosRequest get: ${error.message}`);
      log.error(`get: `, error);
      throw error;
    }
  }

  async post(url: string, data: any, options: AxiosRequestConfig = {}) {
    const response = await axios.post(url, data, options);
    if (response.status === 2) return response;
    else return response.data;
  }
}

export default new AxiosRequestService();
