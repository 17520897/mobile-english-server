import l from "../common/logger";
import { Request, Response, NextFunction } from "express";
import { ErrorsCode } from "../const/errorsCode";

export enum HTTPCode {
  success = 200,
  badRequest = 400,
  notAccept = 406,
  unauthorize = 401,
  serverError = 500,
}

interface IErrors {
  code?: ErrorsCode;
  error?: any;
  field?: any;
}

interface IResponse {
  message?: String;
  data?: any;
  errors?: Array<IErrors>;
}

export class ResponseService {
  send(res: Response, statusCode: HTTPCode, response: IResponse = {}) {
    return res.status(statusCode).send({
      message: response.message,
      data: response.data ? response.data : null,
      errors: response.errors,
      timestamp: Date.now(),
    });
  }
}

export default new ResponseService();
