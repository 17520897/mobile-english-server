import "./common/env";
import Database from "./common/database";
import Server from "./common/server";
import routes from "./routes";

const port = parseInt(process.env.PORT || "3000");

const connectionString =
  process.env.NODE_ENV === "production"
    ? process.env.MONGODB_URI
    : process.env.NODE_ENV === "staging"
      ? process.env.MONGODB_URI_STAGING ||
      "mongodb://localhost:27017/pawn-staging"
      : process.env.MONGODB_URI_DEV ||
      "mongodb://localhost:27017/pawn-dev";
const db = new Database(connectionString);

export default new Server()
  .database(db)
  .router(routes)
  .listen(port)
