import express from "express";
import uploadFile from "../../services/uploadFile";
import uploadFilesController from "./uploadFiles.controller";
import uploadFilesMiddleware from "./uploadFiles.middleware";

const uploadFilesRouter = express.Router();

uploadFilesRouter.post(
  "/images",
  uploadFile.uploadFile(["png", "jpg", "jpeg"]).single("file"),
  uploadFilesMiddleware.uploadImages,
  uploadFilesController.uploadImages
);

export default uploadFilesRouter;
