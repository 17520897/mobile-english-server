import { Request, Response } from "express";
import log4j from "../../common/log4js";
import l from "../../common/logger";
import { ErrorsCode } from "../../const/errorsCode";
import responseService, { HTTPCode } from "../../services/responseService";

const log = log4j("UploadFilesController");
class UploadFilesController {
  async uploadImages(req: Request, res: Response) {
    const { url, originalName } = req.body;
    return responseService.send(res, HTTPCode.success, {
      data: { url, originalName },
      message: "upload file success",
    });
  }
}

export default new UploadFilesController();
