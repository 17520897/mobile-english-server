import { NextFunction, Request, Response } from "express";
import log4j from "../../common/log4js";
import l from "../../common/logger";
import { ErrorsCode } from "../../const/errorsCode";
import responseService, { HTTPCode } from "../../services/responseService";

const log = log4j("UploadFilesMiddleware");
class UploadFilesMiddleware {
  async uploadImages(req: Request, res: Response, next: NextFunction) {
    try {
      const { filename, originalname } = req.file;
      if (filename === "null") {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.uploadFilesOnlyAcceptImageFiles,
              error: "not accept this file - only support: jpg, jpeg, png",
              field: "file",
            },
          ],
        });
      }
      req.body.url = `/${process.env.UPLOAD_DIR}/${filename}`;
      req.body.originalName = req.file.originalname;
      next();
    } catch (error) {
      log.error(`upload image: ${error}`);
      l.error("upload file middleware: ", error.message);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new UploadFilesMiddleware();
