import { Request, Response } from "express";
import log4j from "../../common/log4js";
import l from "../../common/logger";
import responseService, { HTTPCode } from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import axiosRequestService from "../../services/axiosRequestService";
import crawlService from "./crawl.service";
import crawlHistoryService from "../crawlHistory/crawlHistory.service";
const log = log4j("VocabularyController");

export class CrawlController {
  async textToSpeakCrawl(req: Request, res: Response) {
    try {
      const { text } = req.query as any;
      const crawlHistory = await crawlHistoryService.getOne({
        text,
      });
      if (crawlHistory) {
        return responseService.send(res, HTTPCode.success, {
          data: crawlHistory.url,
        });
      }
      const data = await crawlService.crawlTextToSpeech(text);
      if (!data.errors) {
        await crawlHistoryService.create({
          text,
          url: data,
        });
        return responseService.send(res, HTTPCode.success, {
          data: data,
        });
      } else {
        return responseService.send(res, HTTPCode.serverError, {
          errors: data.errors,
        });
      }
    } catch (error) {
      l.error(`textToSpeakCrawl controller: ${error.message}`);
      log.error(`textToSpeakCrawl: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async iconFinderCrawl(req: Request, res: Response) {
    try {
      const { text } = req.query;
      const data = await axiosRequestService.get(
        "https://api.iconfinder.com/v4/icons/search",
        {
          params: {
            query: text,
            count: 20,
          },
          headers: {
            Authorization:
              "Bearer VWRherz0TqKwPlMP5hMLqnHbwHSt639tERLhial2TqiUtVkl6kGyBg6MWeQVQJej",
          },
        }
      );
      const returnData = [];
      if (data.status === 200) {
        const { icons } = data.data;
        const iconsLength = icons.length;
        for (let i = 0; i < iconsLength; i++) {
          if (icons[i].raster_sizes) {
            const { formats } = icons[i].raster_sizes[
              icons[i].raster_sizes.length - 1
            ];
            if (formats && formats[0] && formats[0].preview_url) {
              returnData.push(formats[0].preview_url.toString());
            }
          }
        }
      }
      return responseService.send(res, HTTPCode.success, {
        message: "icon finder success",
        data: returnData,
      });
    } catch (error) {
      l.error(`iconFinderCrawl controller: ${error.message}`);
      log.error(`iconFinder: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async translateCrawl(req: Request, res: Response) {
    try {
      const { text } = req.query;
      const translate = await crawlService.crawlTranslate(text);
      console.log(translate);
      return responseService.send(res, HTTPCode.success, {
        message: "translate success",
        data: translate,
      });
    } catch (error) {
      l.error(`translateCrawl controller: ${error.message}`);
      log.error(`translate: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  // async iconCrawl(req: Request, res: Response) {
  //   try {
  //     const { word } = req.query as any;
  //     // const icons = await crawlIconService.crawl(word);
  //     return responseService.send(res, HTTPCode.success, {
  //       data: icons,
  //       message: "search icons success",
  //     });
  //   } catch (error) {
  //     l.error(`searchIconsVocabulary controller: ${error.message}`);
  //     log.error(`searchIcons: `, error);
  //     return responseService.send(res, HTTPCode.serverError, {
  //       errors: [
  //         {
  //           code: ErrorsCode.serverError,
  //           error: error.message,
  //         },
  //       ],
  //     });
  //   }
  // }
}

export default new CrawlController();
