import l from "../../common/logger";
import axiosRequestService from "../../services/axiosRequestService";
import puppeteer from "puppeteer";
import fs from "fs";
import request from "request";
import cheerio from "cheerio";
class CrawlService {
  async crawlTextToSpeech(text): Promise<any> {
    const data = await axiosRequestService.post(
      "https://texttospeechapi.wideo.co/api/wideo-text-to-speech",
      {
        data: {
          speed: 1,
          text,
          voice: "en-GB-Standard-D",
        },
      }
    );
    console.log("crawlTextToSpeech service: ", data.result);
    if (data.result.url) {
      const now = Date.now();
      await this.saveTTSFile(
        data.result.url,
        `public/audio/${text}-${now}.mp3`
      );
      return `public/audio/${text}-${now}.mp3`;
    } else {
      return {
        errors: [
          {
            error: "can not get audio",
          },
        ],
      };
    }
  }

  async crawlIcons(text, count) {
    const data = await axiosRequestService.get(
      "https://api.iconfinder.com/v4/icons/search",
      {
        params: {
          query: text,
          count,
        },
        headers: {
          Authorization:
            "Bearer VWRherz0TqKwPlMP5hMLqnHbwHSt639tERLhial2TqiUtVkl6kGyBg6MWeQVQJej",
        },
      }
    );
    const returnData = [];
    if (data.status === 200) {
      const { icons } = data.data;
      const iconsLength = icons.length;
      for (let i = 0; i < iconsLength; i++) {
        if (icons[i].raster_sizes) {
          const { formats } = icons[i].raster_sizes[
            icons[i].raster_sizes.length - 1
          ];
          if (formats && formats[0] && formats[0].preview_url) {
            returnData.push(formats[0].preview_url.toString());
          }
        }
      }
      return returnData;
    }
  }

  async crawlTranslate(text) {
    try {
      const URL = `https://dictionary.cambridge.org/vi/dictionary/english-vietnamese/${text}`;
      return new Promise((resolve, reject) => {
        request(URL, async function (error, response, body) {
          if (error) {
            return console.error("There was an error!");
          }
          var $ = cheerio.load(body);
          const vietnamese = $(".trans").contents().first().text();
          const spell = $(".pron-info").contents().first().text();
          const example = $(".eg").first().text();
          resolve({
            vietnamese,
            spell,
            example,
          });
        });
      });
    } catch (error) {
      l.error(error);
    }
  }

  async saveTTSFile(url, filePath) {
    try {
      const data = await axiosRequestService.get(url, {
        responseType: "arraybuffer",
      });
      console.log("saveTTSFile: ", data);
      fs.writeFileSync(`${filePath}`, data.data, { encoding: "binary" });
      return;
    } catch (error) {
      l.error(`test 2: ${error.message}`);
    }
  }
}

export default new CrawlService();
