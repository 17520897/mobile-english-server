import express from "express";
import crawlController from "./crawl.controller";

const crawlRouter = express.Router();

crawlRouter.get("/textToSpeak", crawlController.textToSpeakCrawl);
crawlRouter.get("/icons", crawlController.iconFinderCrawl);
crawlRouter.get("/translate", crawlController.translateCrawl);

export default crawlRouter;
