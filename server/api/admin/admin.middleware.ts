import { Request, Response, NextFunction, response } from "express";
import ResponseService, { HTTPCode } from "../../services/responseService";
import log4j from "../../common/log4js";
import l from "../../common/logger";
import responseService from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import bcrypt from "bcrypt";
import adminService from "./admin.service";
const log = log4j("AdminMiddleware");
class AdminMiddleware {
  async loginAdmin(req: Request, res: Response, next: NextFunction) {
    try {
      const { username, password } = req.body;
      const admin = await adminService.getOne({ username });
      if (!admin) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.adminAdminNotFound,
              error: "admin not found",
              field: "username",
            },
          ],
        });
      }
      const comparePassword = await bcrypt.compare(password, admin.password);
      if (!comparePassword) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.adminWrongPassword,
              error: "wrong password",
              field: "password",
            },
          ],
        });
      }
      req.body.adminId = admin._id;
      next();
    } catch (error) {
      l.error(`loginAdmin middleware: ${error.message}`);
      log.error(`login: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
  async createAdmin(req: Request, res: Response, next: NextFunction) {
    try {
      const { username, phone } = req.body;
      const [checkUsernameExisted, checkPhoneExisted] = await Promise.all([
        adminService.getOne({ username }),
        adminService.getOne({ phone }),
      ]);
      if (checkUsernameExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.adminUsernameExisted,
              field: "username",
              error: "username existed",
            },
          ],
        });
      }
      if (checkPhoneExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.adminPhoneExisted,
              field: "phone",
              error: "phone existed",
            },
          ],
        });
      }
      next();
    } catch (error) {
      l.error(`createAdmin middleware: ${error.message}`);
      log.error(`create: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new AdminMiddleware();
