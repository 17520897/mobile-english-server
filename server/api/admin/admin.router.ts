import express from "express";
import adminController from "./admin.controller";
import adminMiddleware from "./admin.middleware";
import { Valid, RequestType } from "../../middlewares/Valid";
import CreateAdminDto from "./dto/create.dto";
import LoginAdminDto from "./dto/login.dto";
import CheckAccountInfoAdminDto from "./dto/checkAccountInfoAdmin.dto";
import { validateAccount } from "../../middlewares/validate";
import { AccountType } from "../../const";

const router = express.Router();

router.post(
  "/",
  Valid(CreateAdminDto, RequestType.body),
  validateAccount(AccountType.admins),
  adminMiddleware.createAdmin,
  adminController.createAdmin
);

router.post(
  "/login",
  Valid(LoginAdminDto, RequestType.body),
  adminMiddleware.loginAdmin,
  adminController.loginAdmin
);

router.get(
  "/checkAdminAccountInfo",
  Valid(CheckAccountInfoAdminDto, RequestType.query),
  adminController.checkAccountInfoAdmin
);

export default router;
