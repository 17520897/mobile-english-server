import mongoose from "mongoose";

export interface IAdminModel extends mongoose.Document {
  phone: string;
  password: string;
  name: string;
}

const schema = new mongoose.Schema(
  {
    username: {
      type: String,
      trim: true,
      unique: true,
    },
    name: {
      type: String,
    },
    phone: {
      type: String,
      trim: true,
    },
    password: {
      type: String,
    },
  },
  {
    collection: "Admins",
  }
);

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const Admins = mongoose.model<IAdminModel>("Admins", schema);
