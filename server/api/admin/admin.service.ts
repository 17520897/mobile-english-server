import { Admins, IAdminModel } from "./admin.model";
import { QueryFindOneAndRemoveOptions } from "mongoose";

interface IAdminsQuery {
  query?: any;
  page?: any;
  limit?: any;
  sort?: any;
  options?: any;
}

interface IAdminsPopulate {
  query?: any;
  populate?: Array<{
    match?: any;
    path: string;
    select?: string;
    options?: any;
  }>;
  sort?: any;
  select?: string;
  page?: any;
  limit?: any;
}

export class AdminsService {
  async create(data: any): Promise<IAdminModel> {
    const newDoc = new Admins(data);
    const doc = await newDoc.save();
    return doc;
  }

  async getById(id: any): Promise<IAdminModel> {
    const doc = (await Admins.findById(id).lean()) as IAdminModel;
    return doc;
  }

  async getOne(query: any): Promise<IAdminModel> {
    const doc = (await Admins.findOne(query).lean()) as IAdminModel;
    return doc;
  }

  async getSort(query: any, sort: any = {}): Promise<any[]> {
    const doc = await Admins.find(query).sort(sort);
    return doc;
  }

  async getByQuery(iQuery: IAdminsQuery): Promise<any[]> {
    let { page, limit, options, sort, query } = iQuery;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await Admins.find(query, options)
        .sort(sort)
        .skip(page * limit)
        .limit(limit);
      const count = await Admins.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await Admins.find(query, options).sort(sort);
      return doc;
    }
  }

  async deleteOne(query: any): Promise<any> {
    const doc = await Admins.findOneAndDelete(query);
    return doc;
  }

  async deleteById(id: any): Promise<any> {
    const doc = await Admins.findByIdAndDelete(id);
    return doc;
  }

  async deleteMany(
    query: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Admins.deleteMany(query, options);
    return doc;
  }

  async populate(iPopulate: IAdminsPopulate): Promise<any[]> {
    const { query, sort, select, populate } = iPopulate;
    let { page, limit } = iPopulate;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await Admins.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .skip(page * limit)
        .limit(limit)
        .lean()
        .exec();
      const count = await Admins.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await Admins.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .lean()
        .exec();
      return doc;
    }
  }
}

export default new AdminsService();
