import { Request, Response } from "express";
import log4j from "../../common/log4js";
import l from "../../common/logger";
import responseService, { HTTPCode } from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import adminService from "./admin.service";
import bcrypt from "bcrypt";
import jwt from "../../services/jwt";
const log = log4j("AdminController");
export class AdminController {
  async createAdmin(req: Request, res: Response) {
    try {
      const { username, name, phone } = req.body;
      const salt = await bcrypt.genSalt(10);
      const hash = await bcrypt.hash(username, salt);
      const admin = await adminService.create({
        username,
        name,
        password: hash,
        phone,
      });
      admin.password = undefined;
      return responseService.send(res, HTTPCode.success, {
        message: "create admin success",
        data: admin,
      });
    } catch (error) {
      l.error(`createAdmin controller: ${error.message}`);
      log.error(`create: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async loginAdmin(req: Request, res: Response) {
    try {
      const { adminId } = req.body;
      const accessToken = jwt.generateAccessToken({ adminId, isAdmin: true });
      const refreshToken = jwt.generateRefreshToken({ adminId, isAdmin: true });
      return responseService.send(res, HTTPCode.success, {
        data: {
          accessToken,
          refreshToken,
        },
      });
    } catch (error) {
      l.error(`loginAdmin controller: ${error.message}`);
      log.error(`login: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async checkAccountInfoAdmin(req: Request, res: Response) {
    try {
      const { username, phone } = req.query;
      const [checkUsername, checkPhone] = await Promise.all([
        adminService.getOne({
          username: username ? username : "",
        }),
        adminService.getOne({ phone: phone ? phone : "" }),
      ]);
      return responseService.send(res, HTTPCode.success, {
        message: "check admin info success",
        data: {
          isUsernameExisted: checkUsername ? true : false,
          isPhoneExisted: checkPhone ? true : false,
        },
      });
    } catch (error) {
      l.error(`checkAccountInfoAdmin controller: ${error.message}`);
      log.error(`checkAccountInfo: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}
export default new AdminController();
