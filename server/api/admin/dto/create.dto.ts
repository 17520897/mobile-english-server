import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
} from "class-validator";

export default class CreateAdminDto {
  @IsString()
  @Length(10)
  phone: String;

  @IsString()
  username: String;

  @IsString()
  name: String;
}
