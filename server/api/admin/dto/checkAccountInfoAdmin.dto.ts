import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsNumberString,
} from "class-validator";

export default class CheckAccountInfoAdminDto {
  @IsOptional()
  @IsNumberString()
  phone: string;

  @IsOptional()
  @IsString()
  username: string;
}
