import { NextFunction, Request, Response } from "express";
import log4j from "../../common/log4js";
import l from "../../common/logger";
import { ErrorsCode } from "../../const/errorsCode";
import responseService, { HTTPCode } from "../../services/responseService";
import gamesService from "../games/games.service";
import usersService from "../users/users.service";

const log = log4j("UsersHistoryMiddleware");
class UsersHistoryMiddleware {
  async addUsersHistory(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId } = req.cookies;
      const { isAddGamesLevel, isAddVocabularyLevel, gameId } = req.body;
      if (isAddGamesLevel && !gameId) {
        return responseService.send(res, HTTPCode.badRequest, {
          errors: [
            {
              code: ErrorsCode.invalid,
              field: "gameId",
              error: "gameId must be a mongoId",
            },
          ],
        });
      }
      if (isAddGamesLevel) {
        const [checkExistedGameAdd, checkGames] = await Promise.all([
          usersService.getOne({
            _id: userId,
            "gamesPass.game": gameId,
          }),
          gamesService.getById(gameId),
        ]);
        if (checkExistedGameAdd) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.usersHistoryGameIdExisted,
                error: "gameId added",
                field: "gameId",
              },
            ],
          });
        }
        if (!checkGames) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.usersHistoryGameNotFound,
                error: "game not found",
                field: "gameId",
              },
            ],
          });
        }
        req.body.level = {
          level: checkGames.level,
          order: checkGames.order,
        };
      }
      next();
    } catch (error) {
      l.error(`addUsersHistory middleware: ${error.message}`);
      log.error(`add: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new UsersHistoryMiddleware();
