import express from "express";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateAccount } from "../../middlewares/validate";
import AddUsersHistoryDto from "./dto/addUsersHistory.dto";
import GetUsersHistoryDto from "./dto/getUsersHistory.dto";
import usersHistoryController from "./usersHistory.controller";
import usersHistoryMiddleware from "./usersHistory.middleware";

const usersHistoryRouter = express.Router();

usersHistoryRouter.post(
  "/",
  Valid(AddUsersHistoryDto, RequestType.body),
  validateAccount(AccountType.users),
  usersHistoryMiddleware.addUsersHistory,
  usersHistoryController.addUsersHistory
);

usersHistoryRouter.get(
  "/",
  Valid(GetUsersHistoryDto, RequestType.query),
  validateAccount(AccountType.users),
  usersHistoryController.getUsersHistory
);

export default usersHistoryRouter;
