import { Request, Response } from "express";
import log4j from "../../common/log4js";
import l from "../../common/logger";
import { ErrorsCode } from "../../const/errorsCode";
import responseService, { HTTPCode } from "../../services/responseService";
import usersHistoryService from "./usersHistory.service";
import mongoose from "mongoose";
import usersService from "../users/users.service";
const log = log4j("UsersHistoryController");

export class UsersHistoryController {
  async addUsersHistory(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { isAddGamesLevel, isAddVocabularyLevel, gameId } = req.body;
      const { userId } = req.cookies;
      const now = new Date();
      const day = now.getDate();
      const month = now.getMonth();
      const year = now.getFullYear();
      let usersHistory = await usersHistoryService.getOne({
        userId,
        $and: [
          {
            date: {
              $gte: new Date(year, month, day, 0, 0, 0),
            },
          },
          {
            date: { $lte: new Date(year, month, day + 1, 0, 0, 0) },
          },
        ],
      });
      if (!usersHistory) {
        usersHistory = await usersHistoryService.create(
          {
            userId,
            date: now,
          },
          options
        );
      }
      if (isAddGamesLevel) {
        const { level, order } = req.body;
        const users = await usersService.updateById(
          userId,
          {
            $push: { gamesPass: { game: gameId, passAt: Date.now() } },
          },
          options
        );
        console.log(level, order);
        if (
          level > users.levelInfo.level ||
          (level === users.levelInfo.level && order > users.levelInfo.order)
        ) {
          await usersService.updateById(
            userId,
            {
              levelInfo: {
                level,
                order,
              },
            },
            options
          );
        }
        usersHistory.totalGameLevel++;
      }
      if (isAddVocabularyLevel) {
        usersHistory.totalVocabularyPackages++;
      }
      usersHistory = await usersHistoryService.updateById(
        usersHistory._id,
        {
          ...usersHistory,
        },
        options
      );

      await session.commitTransaction();
      return responseService.send(res, HTTPCode.success, {
        data: usersHistory,
      });
    } catch (error) {
      l.error(`addUsersHistory controller: ${error.message}`);
      log.error(`addUsersHistory: `, error);
      await session.abortTransaction();
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    } finally {
      session.endSession();
    }
  }

  async getUsersHistory(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const { beginDate, day } = req.query as any;
      let beginAt = new Date(parseInt(beginDate));
      let endAt = new Date(parseInt(beginDate));
      //đặt lấy tới hết ngày thêm 1
      endAt.setDate(beginAt.getDate() + parseInt(day + 1));
      beginAt.setHours(0, 0, 0, 0);
      //Đặt thời gian lấy tù đầu ngày bắt đầu
      endAt.setHours(0, 0, 0, 0);
      const [usersHistory, totalUsersHistory] = await Promise.all([
        usersHistoryService.getByQuery({
          query: {
            userId,
            $and: [
              {
                date: { $gte: beginAt },
              },
              {
                date: { $lte: endAt },
              },
            ],
          },
        }),
        usersHistoryService.aggregate([
          {
            $group: {
              _id: "1",
              totalGameLevel: { $sum: "$totalGameLevel" },
              totalVocabularyPackages: { $sum: "$totalVocabularyPackages" },
              loginDay: { $sum: 1 },
            },
          },
        ]),
      ]);
      if (totalUsersHistory.length > 0)
        usersHistory.push({
          totalGameLevel: totalUsersHistory[0].totalGameLevel,
          totalVocabularyPackages: totalUsersHistory[0].totalVocabularyPackages,
          loginDay: totalUsersHistory[0].loginDay,
        });
      responseService.send(res, HTTPCode.success, {
        data: usersHistory,
        message: "getUsersHistory",
      });
    } catch (error) {
      l.error(`getUsersHistory controller: ${error.message}`);
      log.error(`getUsers: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new UsersHistoryController();
