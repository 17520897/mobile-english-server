import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsBoolean,
  NotEquals,
  Equals,
  IsDateString,
  IsNumberString,
} from "class-validator";

export default class GetUsersHistoryDto {
  @IsString()
  beginDate: string;

  @IsNumberString()
  day: string;
}
