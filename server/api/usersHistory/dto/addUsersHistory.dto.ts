import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsBoolean,
  NotEquals,
  Equals,
  IsDateString,
} from "class-validator";

export default class AddUsersHistoryDto {
  @IsOptional()
  @IsBoolean()
  isAddGamesLevel: boolean;

  @IsOptional()
  @IsBoolean()
  isAddVocabularyLevel: boolean;
}
