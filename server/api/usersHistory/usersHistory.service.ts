import { UsersHistory, IUsersHistoryModel } from "./usersHistory.model";
import { Aggregate, QueryFindOneAndRemoveOptions, SaveOptions } from "mongoose";

interface IUsersHistoryQuery {
  query?: any;
  page?: any;
  limit?: any;
  sort?: any;
  options?: any;
}

interface IUsersHistoryPopulate {
  query?: any;
  populate?: Array<{
    match?: any;
    path: string;
    select?: string;
    options?: any;
  }>;
  sort?: any;
  select?: string;
  page?: any;
  limit?: any;
}

export class UsersHistoryService {
  async create(
    data: any,
    options: SaveOptions = {}
  ): Promise<IUsersHistoryModel> {
    const newDoc = new UsersHistory(data);
    const doc = await newDoc.save(options);
    return doc;
  }

  async getById(id: any): Promise<IUsersHistoryModel> {
    const doc = (await UsersHistory.findById(id).lean()) as IUsersHistoryModel;
    return doc;
  }

  async getOne(query: any): Promise<IUsersHistoryModel> {
    const doc = (await UsersHistory.findOne(
      query
    ).lean()) as IUsersHistoryModel;
    return doc;
  }

  async getSort(query: any, sort: any = {}): Promise<any[]> {
    const doc = await UsersHistory.find(query).sort(sort);
    return doc;
  }

  async getByQuery(iQuery: IUsersHistoryQuery): Promise<any[]> {
    let { page, limit, options, sort, query } = iQuery;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await UsersHistory.find(query, options)
        .sort(sort)
        .skip(page * limit)
        .limit(limit);
      const count = await UsersHistory.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await UsersHistory.find(query, options).sort(sort);
      return doc;
    }
  }

  async deleteOne(query: any): Promise<any> {
    const doc = await UsersHistory.findOneAndDelete(query);
    return doc;
  }

  async deleteById(
    id: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await UsersHistory.findByIdAndDelete(id, options);
    return doc;
  }

  async deleteMany(
    query: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await UsersHistory.deleteMany(query, options);
    return doc;
  }

  async populate(iPopulate: IUsersHistoryPopulate): Promise<any[]> {
    const { query, sort, select, populate } = iPopulate;
    let { page, limit } = iPopulate;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await UsersHistory.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .skip(page * limit)
        .limit(limit)
        .lean()
        .exec();
      const count = await UsersHistory.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await UsersHistory.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .lean()
        .exec();
      return doc;
    }
  }

  async updateOne(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<IUsersHistoryModel> {
    const doc = await UsersHistory.findOneAndUpdate(query, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateById(
    id,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<IUsersHistoryModel> {
    const doc = await UsersHistory.findByIdAndUpdate(id, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateMany(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await UsersHistory.updateMany(query, data, options);
    return doc;
  }

  async aggregate(query: Array<any>): Promise<any> {
    const doc = await UsersHistory.aggregate(query);
    return doc;
  }
}

export default new UsersHistoryService();
