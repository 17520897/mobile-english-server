import mongoose from "mongoose";

export interface IUsersHistoryModel extends mongoose.Document {
  userId: string;
  date: Date;
  totalGameLevel: number;
  totalVocabularyPackages: number;
}

const schema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    date: {
      type: Date,
    },
    totalGameLevel: {
      type: Number,
      default: 0,
    },
    totalVocabularyPackages: {
      type: Number,
      default: 0,
    },
    isLogin: {
      type: Boolean,
      default: true,
    },
  },
  {
    collection: "UsersHistory",
  }
);

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const UsersHistory = mongoose.model<IUsersHistoryModel>(
  "UsersHistory",
  schema
);
