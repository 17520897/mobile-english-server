import {
  VocabularyPackages,
  IVocabularyPackagesModel,
} from "./vocabularyPackages.model";
import { QueryFindOneAndRemoveOptions } from "mongoose";

interface IVocabularyPackagesQuery {
  query?: any;
  page?: any;
  limit?: any;
  sort?: any;
  options?: any;
}

interface IVocabularyPackagesPopulate {
  query?: any;
  populate?: Array<{
    match?: any;
    path: string;
    select?: string;
    options?: any;
  }>;
  sort?: any;
  select?: string;
  page?: any;
  limit?: any;
}

export class VocabularyPackagesService {
  async create(
    data: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<IVocabularyPackagesModel> {
    const newDoc = new VocabularyPackages(data);
    const doc = await newDoc.save(options);
    return doc;
  }

  async createOrUpdate(
    query: any,
    data: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<IVocabularyPackagesModel> {
    const doc = await VocabularyPackages.findOneAndUpdate(query, data, {
      new: true,
      upsert: true,
      ...options,
    });
    return doc;
  }

  async getById(id: any): Promise<IVocabularyPackagesModel> {
    const doc = (await VocabularyPackages.findById(
      id
    ).lean()) as IVocabularyPackagesModel;
    return doc;
  }

  async getOne(query: any): Promise<IVocabularyPackagesModel> {
    const doc = (await VocabularyPackages.findOne(
      query
    ).lean()) as IVocabularyPackagesModel;
    return doc;
  }

  async getSort(query: any, sort: any = {}): Promise<any[]> {
    const doc = await VocabularyPackages.find(query).sort(sort);
    return doc;
  }

  async getByQuery(iQuery: IVocabularyPackagesQuery): Promise<any[]> {
    let { page, limit, options, sort, query } = iQuery;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await VocabularyPackages.find(query, options)
        .sort(sort)
        .skip(page * limit)
        .limit(limit);
      const count = await VocabularyPackages.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await VocabularyPackages.find(query, options).sort(sort);
      return doc;
    }
  }

  async deleteOne(query: any): Promise<any> {
    const doc = await VocabularyPackages.findOneAndDelete(query);
    return doc;
  }

  async deleteById(
    id: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await VocabularyPackages.findByIdAndDelete(id, options);
    return doc;
  }

  async deleteMany(
    query: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await VocabularyPackages.deleteMany(query, options);
    return doc;
  }

  async populate(iPopulate: IVocabularyPackagesPopulate): Promise<any[]> {
    const { query, sort, select, populate } = iPopulate;
    let { page, limit } = iPopulate;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await VocabularyPackages.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .skip(page * limit)
        .limit(limit)
        .lean()
        .exec();
      const count = await VocabularyPackages.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await VocabularyPackages.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .lean()
        .exec();
      return doc;
    }
  }

  async updateOne(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<IVocabularyPackagesModel> {
    const doc = await VocabularyPackages.findOneAndUpdate(query, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateById(
    id,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<IVocabularyPackagesModel> {
    const doc = await VocabularyPackages.findByIdAndUpdate(id, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateMany(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await VocabularyPackages.updateMany(query, data, options);
    return doc;
  }
}

export default new VocabularyPackagesService();
