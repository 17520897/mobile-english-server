import { Request, Response, NextFunction } from "express";
import log4j from "../../common/log4js";
import l from "../../common/logger";
import responseService, { HTTPCode } from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import vocabularyPackagesService from "./vocabularyPackages.service";
const log = log4j("VocabularyPackagesMiddleware");
export class VocabularyPackagesMiddleware {
  async createVocabularyPackages(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { parent, ancestors, name } = req.body;
      if (ancestors) {
        const ancestorsLength = ancestors.length;
        for (let i = ancestorsLength - 1; i >= 0; i--) {
          if (
            i === ancestorsLength - 1 &&
            ancestors[i].toString() !== parent.toString() //Kiểm tra parent có trùng với id cuối cùng đc nằm trong danh sách gia phả của cây.
          ) {
            return responseService.send(res, HTTPCode.notAccept, {
              errors: [
                {
                  code:
                    ErrorsCode.vocabularyPackageLastDataInAncestorMustBeEquaParent,
                  error: "last data in ancestors must equal parent",
                  field: "ancestors",
                },
              ],
            });
          } else if (i > 0) {
            const checkAncestors = await vocabularyPackagesService.getById(
              ancestors[i]
            );
            if (
              !checkAncestors ||
              checkAncestors.parent.toString() !== ancestors[i - 1].toString() //Kiểm tra xem ancestors có truyền đúng theo danh sách từ root đi vào.
            ) {
              return responseService.send(res, HTTPCode.notAccept, {
                errors: [
                  {
                    code:
                      ErrorsCode.vocabularyPackageAncestorsNotHaveCorrectFormat,
                    error: "ancestors not have correct format",
                    field: "ancestors",
                  },
                ],
              });
            }
          }
        }
      }
      next();
    } catch (error) {
      l.error(`createVocabularyPackages middleware: ${error.message}`);
      log.error(`create: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new VocabularyPackagesMiddleware();
