import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsBoolean,
  NotEquals,
  Equals,
  IsDateString,
  IsNumberString,
} from "class-validator";

export default class UpdateVocabularyPackagesDto {
  @Equals(undefined)
  parent: string;

  @Equals(undefined)
  ancestors: string;
}
