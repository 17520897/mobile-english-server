import { Request, Response } from "express";
import l from "../../common/logger";
import log4j from "../../common/log4js";
import responseService, { HTTPCode } from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import vocabularyPackagesService from "./vocabularyPackages.service";
import mongoose from "mongoose";
const log = log4j("VocabularyPackagesController");
export class VocabularyPackagesController {
  async createVocabularyPackages(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { name, ancestors, parent, vocabularies, packageImage } = req.body;
      if (packageImage) {
        const packageImageSplit = packageImage.split("/");
        if (packageImageSplit[1] === "public") {
          req.body.packageImage = `http://staging.pd-mobile-web.tk${packageImage}`;
        }
      }
      const vocabularyPackage = await vocabularyPackagesService.createOrUpdate(
        {
          name,
        },
        req.body,
        options
      ); // tạo package
      await vocabularyPackagesService.updateById(
        parent,
        { isLeaf: false },
        options
      ); //Chuyển cha của thằng mới được tạo thành không phải con cuối cùng.
      await session.commitTransaction();
      return responseService.send(res, HTTPCode.success, {
        message: "create vocabulary package success",
        data: vocabularyPackage,
      });
    } catch (error) {
      await session.abortTransaction();
      l.error(`createVocabularyPackages controller: ${error.message}`);
      log.error(`create: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    } finally {
      session.endSession();
    }
  }

  async updateVocabularyPackages(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const vocabularyPackage = await vocabularyPackagesService.updateById(
        id,
        req.body
      );
      return responseService.send(res, HTTPCode.success, {
        data: vocabularyPackage,
        message: "update vocabulary packages success",
      });
    } catch (error) {
      l.error(`updateVocabularyPackages controller: ${error.message}`);
      log.error(`update: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async getVocabularyPackages(req: Request, res: Response) {
    try {
      const { page, limit, root, parent, onlyChild } = req.query;
      let query: any;
      if (root === "true") {
        query = {
          $or: [
            {
              parent: { $exists: false },
            },
            {
              parent: null,
            },
          ],
        };
      } else if (onlyChild === "true") {
        query = {
          isLeaf: true,
        };
      } else if (parent) {
        query = {
          parent,
        };
      }
      const vocabularyPackage = await vocabularyPackagesService.populate({
        query,
        page,
        limit,
        populate: [
          {
            path: "parent",
            select: "name",
          },
          {
            path: "vocabularies.vocabulary",
          },
        ],
      });
      return responseService.send(res, HTTPCode.success, {
        message: "get vocabulary package success",
        data: vocabularyPackage,
      });
    } catch (error) {
      l.error(`getRootVocabularyPackages controller: ${error.message}`);
      log.error(`get: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async deleteVocabularyPackages(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await vocabularyPackagesService.deleteById(id);
      return responseService.send(res, HTTPCode.success, {
        message: "delete vocabulary packages success",
      });
    } catch (error) {
      l.error(`deleteVocabularyPackages controller: ${error.message}`);
      log.error(`delete: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new VocabularyPackagesController();
