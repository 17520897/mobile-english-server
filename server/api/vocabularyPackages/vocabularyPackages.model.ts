import mongoose from "mongoose";

export interface IVocabularyPackagesModel extends mongoose.Document {
  name: string;
  ancestors: Array<string>;
  parent: string;
  vocabularies: Array<{
    vocabulary: string;
  }>;
  packageImage: string;
}

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
    },
    packageImage: {
      type: String,
      trim: true,
      default:
        "https://cdn3.iconfinder.com/data/icons/fantasy-and-role-play-game-adventure-quest/512/Dragon_Egg-512.png",
    },
    ancestors: [], //các id tổ tiên của packages này.
    parent: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    vocabularies: [
      {
        vocabulary: {
          type: mongoose.SchemaTypes.ObjectId,
          ref: "Vocabulary",
        },
      },
    ],
    isLeaf: {
      type: Boolean,
      default: true,
    },
  },
  {
    collection: "VocabularyPackages",
  }
);

schema.virtual("parentInfo", {
  ref: "VocabularyPackages",
  localField: "parentId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const VocabularyPackages = mongoose.model<IVocabularyPackagesModel>(
  "VocabularyPackages",
  schema
);
