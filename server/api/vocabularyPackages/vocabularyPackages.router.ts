import express from "express";
import vocabularyPackagesController from "./vocabularyPackages.controller";
import vocabularyPackagesMiddleware from "./vocabularyPackages.middleware";
import { validateAccount } from "../../middlewares/validate";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import UpdateVocabularyPackagesDto from "./dto/updateVocabularyPackages.dto";

const vocabularyPackagesRouter = express.Router();

vocabularyPackagesRouter.get(
  "/",
  vocabularyPackagesController.getVocabularyPackages
);

vocabularyPackagesRouter.post(
  "/",
  validateAccount(AccountType.admins),
  vocabularyPackagesMiddleware.createVocabularyPackages,
  vocabularyPackagesController.createVocabularyPackages
);

vocabularyPackagesRouter.put(
  "/:id",
  Valid(UpdateVocabularyPackagesDto, RequestType.body),
  validateAccount(AccountType.admins),
  vocabularyPackagesMiddleware.createVocabularyPackages,
  vocabularyPackagesController.createVocabularyPackages
);

vocabularyPackagesRouter.delete(
  "/:id",
  validateAccount(AccountType.admins),
  vocabularyPackagesController.deleteVocabularyPackages
);

export default vocabularyPackagesRouter;
