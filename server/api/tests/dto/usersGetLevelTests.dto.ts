import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsBoolean,
  NotEquals,
  Equals,
  IsDateString,
  IsArray,
  IsNumberString,
} from "class-validator";

export default class UsersGetLevelTestsDto {
  @IsNumberString()
  level: string;
}
