import mongoose from "mongoose";

export interface ITestsModel extends mongoose.Document {
  name: string;
  level: number;
  questions: Array<{
    type: string;
    question: string;
    image: string;
    link: string;
    answers: Array<{
      answer: string;
      image: string;
      audio: string;
    }>;
    rightAnswer: string;
  }>;
  createAt: Date;
}

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    level: {
      type: Number,
      required: true,
      default: 1,
    },
    questions: [
      {
        type: {
          type: String,
          enum: ["pictures", "listening", "sentence"],
        },
        question: {
          type: String,
        },
        image: {
          type: String,
        },
        link: {
          type: String,
        },
        answers: [
          {
            answer: {
              type: String,
            },
            image: {
              type: String,
            },
            audio: {
              type: String,
            },
          },
        ],
        rightAnswer: {
          type: String,
        },
      },
    ],
    creatAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: "Tests",
  }
);

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const Tests = mongoose.model<ITestsModel>("Tests", schema);
