import { Tests, ITestsModel } from "./tests.model";
import { QueryFindOneAndRemoveOptions, SaveOptions } from "mongoose";

interface ITestsQuery {
  query?: any;
  page?: any;
  limit?: any;
  sort?: any;
  options?: any;
}

interface ITestsPopulate {
  query?: any;
  populate?: Array<{
    match?: any;
    path: string;
    select?: string;
    options?: any;
  }>;
  sort?: any;
  select?: string;
  page?: any;
  limit?: any;
}

export class TestsService {
  async create(data: any, options: SaveOptions = {}): Promise<ITestsModel> {
    const newDoc = new Tests(data);
    const doc = await newDoc.save();
    return doc;
  }

  async createOrUpdate(query: any, data: any): Promise<ITestsModel> {
    const newDoc = await Tests.findOneAndUpdate(query, data, {
      new: true,
      upsert: true,
    });
    return newDoc;
  }

  async getById(id: any): Promise<ITestsModel> {
    const doc = (await Tests.findById(id).lean()) as ITestsModel;
    return doc;
  }

  async getOne(query: any): Promise<ITestsModel> {
    const doc = (await Tests.findOne(query).lean()) as ITestsModel;
    return doc;
  }

  async getSort(query: any, sort: any = {}): Promise<any[]> {
    const doc = await Tests.find(query).sort(sort);
    return doc;
  }

  async getByQuery(iQuery: ITestsQuery): Promise<any[]> {
    let { page, limit, options, sort, query } = iQuery;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await Tests.find(query, options)
        .sort(sort)
        .skip(page * limit)
        .limit(limit);
      const count = await Tests.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await Tests.find(query, options).sort(sort);
      return doc;
    }
  }

  async deleteOne(
    query: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Tests.findOneAndDelete(query, options);
    return doc;
  }

  async deleteById(
    id: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Tests.findByIdAndDelete(id, options);
    return doc;
  }

  async deleteMany(
    query: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Tests.deleteMany(query, options);
    return doc;
  }

  async populate(iPopulate: ITestsPopulate): Promise<any[]> {
    const { query, sort, select, populate } = iPopulate;
    let { page, limit } = iPopulate;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await Tests.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .skip(page * limit)
        .limit(limit)
        .lean()
        .exec();
      const count = await Tests.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await Tests.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .lean()
        .exec();
      return doc;
    }
  }

  async updateOne(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<ITestsModel> {
    const doc = await Tests.findOneAndUpdate(query, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateById(
    id,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<ITestsModel> {
    const doc = await Tests.findByIdAndUpdate(id, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateMany(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Tests.updateMany(query, data, options);
    return doc;
  }
}

export default new TestsService();
