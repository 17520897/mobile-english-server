import express from "express";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateAccount } from "../../middlewares/validate";
import IdParamsTestsDto from "./dto/idParamsTests.dto";
import UsersGetLevelTestsDto from "./dto/usersGetLevelTests.dto";
import testsController from "./tests.controller";
import testsMiddleware from "./tests.middleware";

const testsRouter = express.Router();

testsRouter.put(
  "/:id",
  Valid(IdParamsTestsDto, RequestType.params),
  validateAccount(AccountType.admins),
  testsController.updateTests
);

testsRouter.get(
  "/list",
  validateAccount(AccountType.admins),
  testsController.getListTests
);

testsRouter.get(
  "/",
  Valid(UsersGetLevelTestsDto, RequestType.query),
  validateAccount(AccountType.users),
  testsMiddleware.usersGetTestLevelTests,
  testsController.usersGetTestLevelTests
);

testsRouter.put(
  "/finish/:id",
  Valid(IdParamsTestsDto, RequestType.params),
  validateAccount(AccountType.users),
  testsMiddleware.usersFinishTestLevelTests,
  testsController.usersFinishTestLevelTests
);

export default testsRouter;
