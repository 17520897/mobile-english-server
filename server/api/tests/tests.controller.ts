import { Request, Response, response } from "express";
import l from "../../common/logger";
import log4j from "../../common/log4js";
import responseService, { HTTPCode } from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import testsService from "./tests.service";
import usersService from "../users/users.service";
import vocabularyService from "../vocabulary/vocabulary.service";

const log = log4j("TestsController");
class TestsController {
  async updateTests(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { questions } = req.body;
      const { updateType } = req.query;
      for (let i = 0; i < questions.length; i++) {
        if (questions[i].type !== "sentence") {
          const rightAnswer = await vocabularyService.getById(
            questions[i].rightAnswer
          );
          questions[i].rightAnswer = rightAnswer.english;
        }
        const answers = [...questions[i].answers];
        questions[i].answers = [];
        for (let j = 0; j < answers.length; j++) {
          const answer = await vocabularyService.getById(answers[j]);
          questions[i].answers.push({
            answer: answer.english,
            image: answer.image,
            audio: answer.audio,
          });
        }
      }
      req.body.questions = questions;
      const tests = await testsService.updateById(id, req.body);
      return responseService.send(res, HTTPCode.success, {
        message: "create or update tests success",
        data: tests,
      });
    } catch (error) {
      l.error(`updateTests controller: ${error.message}`);
      log.error(`update: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async getListTests(req: Request, res: Response) {
    try {
      const { page, limit } = req.query;
      const tests = await testsService.getByQuery({
        page,
        limit,
        sort: {
          level: 1,
        },
      });
      return responseService.send(res, HTTPCode.success, {
        data: tests,
        message: "get list tests",
      });
    } catch (error) {
      l.error(`getListTests controller: ${error.message}`);
      log.error(`getListTests: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async usersGetTestLevelTests(req: Request, res: Response) {
    try {
      const { tests } = req.body;
      return responseService.send(res, HTTPCode.success, {
        message: "users get test with level success",
        data: tests,
      });
    } catch (error) {
      l.error(`usersGetTestLevelTests controller: ${error.message}`);
      log.error(`usersGetTestLevel: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async usersFinishTestLevelTests(req: Request, res: Response) {
    try {
      const { test } = req.body; //from middleware;
      const { userId } = req.cookies;
      const user = await usersService.getById(userId);
      if (user.levelInfo.level <= test.level) {
        await usersService.updateById(userId, {
          levelInfo: {
            level: test.level + 1,
            order: 0,
          },
        });
      }
      responseService.send(res, HTTPCode.success, {
        message: "finish test success",
      });
    } catch (error) {
      l.error(`usersFinishTestLevelTests controller: ${error.message}`);
      log.error(`usersFinishTestLevel: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new TestsController();
