import { NextFunction, Request, Response } from "express";
import log4j from "../../common/log4js";
import l from "../../common/logger";
import { ErrorsCode } from "../../const/errorsCode";
import responseService, { HTTPCode } from "../../services/responseService";
import testsService from "./tests.service";

const log = log4j("TestsMiddleware");
class TestsMiddleware {
  async usersGetTestLevelTests(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { level } = req.query as any;
      const tests = await testsService.getOne({
        level: parseInt(level),
      });
      if (!tests) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.testsTestNotFound,
              field: "level",
              error: "test not found",
            },
          ],
        });
      }
      req.body.tests = tests;
      next();
    } catch (error) {
      l.error(`usersGetTestsLevelTests middleware: ${error.message}`);
      log.error(`usersGetTestsLevel: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
  async usersFinishTestLevelTests(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { id } = req.params;
      const test = await testsService.getById(id);
      if (!test) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.testsTestNotFound,
              field: "id",
              error: "test not found",
            },
          ],
        });
      }
      req.body.test = test;
      next();
    } catch (error) {
      l.error(`usersFinishTestLevelTests middleware: ${error.message}`);
      log.error(`usersFinishTestLevel: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new TestsMiddleware();
