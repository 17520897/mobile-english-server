import express from "express";
import vocabularyController from "./vocabulary.controller";
import { validateAccount } from "../../middlewares/validate";
import { AccountType } from "../../const";
import vocabularyMiddleware from "./vocabulary.middleware";

const vocabularyRouter = express.Router();

vocabularyRouter.post(
  "/",
  vocabularyMiddleware.createVocabulary,
  vocabularyController.createVocabulary
);

vocabularyRouter.get("/", vocabularyController.getVocabulary);

vocabularyRouter.delete(
  "/:id",
  validateAccount(AccountType.admins),
  vocabularyController.deleteVocabulary
);

vocabularyRouter.get("/search", vocabularyController.searchVocabulary);

export default vocabularyRouter;
