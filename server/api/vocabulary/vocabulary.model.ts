import mongoose from "mongoose";

export interface IVocabularyModel extends mongoose.Document {
  english: string;
  vietnamese: string;
  image: string;
  spell: string;
  audio: string;
  explain: string;
  example: string;
  exampleTranslate: string;
}

const schema = new mongoose.Schema(
  {
    english: {
      type: String,
      trim: true,
      required: true,
    },
    vietnamese: {
      type: String,
      trim: true,
      required: true,
    },
    image: {
      type: String,
      trim: true,
    },
    spell: {
      type: String,
      trim: true,
    },
    audio: {
      type: String,
      trim: true,
    },
    explain: {
      type: String,
      trim: true,
    },
    example: {
      type: String,
      trim: true,
    },
    exampleTranslate: {
      type: String,
      trim: true,
    },
  },
  {
    collection: "Vocabulary",
  }
);

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const Vocabulary = mongoose.model<IVocabularyModel>(
  "Vocabulary",
  schema
);
