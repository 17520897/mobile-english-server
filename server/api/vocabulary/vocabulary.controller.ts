import { Request, Response } from "express";
import log4j from "../../common/log4js";
import l from "../../common/logger";
import responseService, { HTTPCode } from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import vocabularyService from "./vocabulary.service";
import mongoose from "mongoose";
import vocabularyPackagesService from "../vocabularyPackages/vocabularyPackages.service";
import axiosRequestService from "../../services/axiosRequestService";
import crawlIconService from "../../services/crawlIconService";
const log = log4j("VocabularyController");
export class VocabularyController {
  async createVocabulary(req: Request, res: Response) {
    try {
      const {
        english,
        vietnamese,
        audio,
        image,
        spell,
        explain,
        example,
        exampleTranslate,
      } = req.body;
      const vocabulary = await vocabularyService.create({
        english,
        vietnamese,
        audio,
        image,
        spell,
        explain,
        example,
        exampleTranslate,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "create vocabulary success",
        data: vocabulary,
      });
    } catch (error) {
      l.error(`createVocabulary controller: ${error.message}`);
      log.error(`create: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async getVocabulary(req: Request, res: Response) {
    try {
      const { page, limit, vocabularyId } = req.query;
      let query = {} as any;
      let vocabulary;
      if (vocabularyId) {
        query = {
          _id: vocabularyId,
        };
        vocabulary = await vocabularyService.getById(vocabularyId);
      } else {
        vocabulary = await vocabularyService.getByQuery({
          page,
          limit,
        });
      }
      return responseService.send(res, HTTPCode.success, {
        data: vocabulary,
        message: "get vocabulary success",
      });
    } catch (error) {
      l.error(`getVocabulary controller: ${error.message}`);
      log.error(`get: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async deleteVocabulary(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const { id } = req.params;
      const options = { session };
      await vocabularyPackagesService.updateMany(
        { "vocabularies.vocabulary": id },
        {
          $pull: {
            vocabularies: id,
          },
        },
        options
      );
      await vocabularyService.deleteById(id, options);
      await session.commitTransaction();
      return responseService.send(res, HTTPCode.success, {
        message: "delete vocabulary success",
      });
    } catch (error) {
      l.error(`deleteVocabulary controller: ${error.message}`);
      log.error(`delete: `, error);
      await session.abortTransaction();
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    } finally {
      session.endSession();
    }
  }

  async searchVocabulary(req: Request, res: Response) {
    try {
      const { word, page, limit } = req.query;
      const vocabulary = await vocabularyService.getByQuery({
        query: {
          $or: [
            {
              english: { $regex: word, $options: "i" },
            },
            {
              vietnamese: { $regex: word, $options: "i" },
            },
          ],
        },
        page,
        limit,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "search vocabulary success",
        data: vocabulary,
      });
    } catch (error) {
      l.error(`searchVocabulary controller: ${error.message}`);
      log.error(`search: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new VocabularyController();
