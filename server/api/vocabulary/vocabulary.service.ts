import { Vocabulary, IVocabularyModel } from "./vocabulary.model";
import { QueryFindOneAndRemoveOptions } from "mongoose";

interface IVocabularyQuery {
  query?: any;
  page?: any;
  limit?: any;
  sort?: any;
  options?: any;
}

interface IVocabularyPopulate {
  query?: any;
  populate?: Array<{
    match?: any;
    path: string;
    select?: string;
    options?: any;
  }>;
  sort?: any;
  select?: string;
  page?: any;
  limit?: any;
}

export class VocabularyService {
  async create(data: any): Promise<IVocabularyModel> {
    const newDoc = new Vocabulary(data);
    const doc = await newDoc.save();
    return doc;
  }

  async getById(id: any): Promise<IVocabularyModel> {
    const doc = (await Vocabulary.findById(id).lean()) as IVocabularyModel;
    return doc;
  }

  async getOne(query: any): Promise<IVocabularyModel> {
    const doc = (await Vocabulary.findOne(query).lean()) as IVocabularyModel;
    return doc;
  }

  async getSort(query: any, sort: any = {}): Promise<any[]> {
    const doc = await Vocabulary.find(query).sort(sort);
    return doc;
  }

  async getByQuery(iQuery: IVocabularyQuery): Promise<any[]> {
    let { page, limit, options, sort, query } = iQuery;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await Vocabulary.find(query, options)
        .sort(sort)
        .skip(page * limit)
        .limit(limit);
      const count = await Vocabulary.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await Vocabulary.find(query, options).sort(sort);
      return doc;
    }
  }

  async deleteOne(query: any): Promise<any> {
    const doc = await Vocabulary.findOneAndDelete(query);
    return doc;
  }

  async deleteById(
    id: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Vocabulary.findByIdAndDelete(id, options);
    return doc;
  }

  async deleteMany(
    query: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Vocabulary.deleteMany(query, options);
    return doc;
  }

  async populate(iPopulate: IVocabularyPopulate): Promise<any[]> {
    const { query, sort, select, populate } = iPopulate;
    let { page, limit } = iPopulate;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await Vocabulary.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .skip(page * limit)
        .limit(limit)
        .lean()
        .exec();
      const count = await Vocabulary.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await Vocabulary.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .lean()
        .exec();
      return doc;
    }
  }

  async updateOne(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<IVocabularyModel> {
    const doc = await Vocabulary.findOneAndUpdate(query, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateById(
    id,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<IVocabularyModel> {
    const doc = await Vocabulary.findByIdAndUpdate(id, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateMany(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Vocabulary.updateMany(query, data, options);
    return doc;
  }
}

export default new VocabularyService();
