import { NextFunction, Request, Response } from "express";
import log4j from "../../common/log4js";
import l from "../../common/logger";
import { ErrorsCode } from "../../const/errorsCode";
import responseService, { HTTPCode } from "../../services/responseService";
import vocabularyService from "./vocabulary.service";

const log = log4j("VocabularyMiddleware");
class VocabularyMiddleware {
  async createVocabulary(req: Request, res: Response, next: NextFunction) {
    try {
      const { english } = req.body;
      const checkVocabularyExisted = await vocabularyService.getOne({
        english,
      });
      if (checkVocabularyExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.vocabularyWordsExisted,
              field: "english",
              error: "words existed",
            },
          ],
        });
      }
      next();
    } catch (error) {
      l.error(`createVocabulary middleware: ${error.message}`);
      log.error(`create: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new VocabularyMiddleware();
