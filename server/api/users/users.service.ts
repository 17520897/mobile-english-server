import { Users, IUsersModel } from "./users.model";
import { QueryFindOneAndRemoveOptions, SaveOptions } from "mongoose";

interface IUsersQuery {
  query?: any;
  page?: any;
  limit?: any;
  sort?: any;
  options?: any;
}

interface IUsersPopulate {
  query?: any;
  populate?: Array<{
    match?: any;
    path: string;
    select?: string;
    options?: any;
  }>;
  sort?: any;
  select?: string;
  page?: any;
  limit?: any;
}

export class UsersService {
  async create(data: any, options: SaveOptions = {}): Promise<IUsersModel> {
    const newDoc = new Users(data);
    const doc = await newDoc.save(options);
    return doc;
  }

  async getById(id: any): Promise<IUsersModel> {
    const doc = (await Users.findById(id).lean()) as IUsersModel;
    return doc;
  }

  async getOne(query: any): Promise<IUsersModel> {
    const doc = (await Users.findOne(query).lean()) as IUsersModel;
    return doc;
  }

  async getSort(query: any, sort: any = {}): Promise<any[]> {
    const doc = await Users.find(query).sort(sort);
    return doc;
  }

  async getByQuery(iQuery: IUsersQuery): Promise<any[]> {
    let { page, limit, options, sort, query } = iQuery;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await Users.find(query, options)
        .sort(sort)
        .skip(page * limit)
        .limit(limit);
      const count = await Users.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await Users.find(query, options).sort(sort);
      return doc;
    }
  }

  async deleteOne(query: any): Promise<any> {
    const doc = await Users.findOneAndDelete(query);
    return doc;
  }

  async deleteById(
    id: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Users.findByIdAndDelete(id, options);
    return doc;
  }

  async deleteMany(
    query: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Users.deleteMany(query, options);
    return doc;
  }

  async populate(iPopulate: IUsersPopulate): Promise<any[]> {
    const { query, sort, select, populate } = iPopulate;
    let { page, limit } = iPopulate;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await Users.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .skip(page * limit)
        .limit(limit)
        .lean()
        .exec();
      const count = await Users.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await Users.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .lean()
        .exec();
      return doc;
    }
  }

  async updateOne(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<IUsersModel> {
    const doc = await Users.findOneAndUpdate(query, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateById(
    id,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<IUsersModel> {
    const doc = await Users.findByIdAndUpdate(id, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateMany(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Users.updateMany(query, data, options);
    return doc;
  }
}

export default new UsersService();
