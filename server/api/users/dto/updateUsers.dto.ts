import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsBoolean,
  NotEquals,
  Equals,
  IsDateString,
} from "class-validator";
enum GenderUsers {
  male = "male",
  female = "female",
}

export default class UpdateUsersDto {
  @Equals(undefined)
  email: string;

  @Equals(undefined)
  googleEmail: string;

  @IsOptional()
  @IsString()
  password: string;

  @IsOptional()
  @IsString()
  oldPassword: string;

  @IsOptional()
  @IsEnum(GenderUsers)
  gender: string;

  @IsOptional()
  @IsDateString()
  dayOfBirth: Date;

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  avatar: string;

  @Equals(undefined)
  status: string;
}
