import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsBoolean,
  NotEquals,
  Equals,
  IsDateString,
} from "class-validator";

enum GenderUsers {
  male = "male",
  female = "female",
}

export default class CreateUsersDto {
  @IsEmail()
  email: string;

  @Equals(undefined)
  googleEmail: string;

  @IsString()
  password: string;

  @IsOptional()
  @IsEnum(GenderUsers)
  gender: string;

  @IsOptional()
  @IsDateString()
  dayOfBirth: Date;

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  avatar: string;
}
