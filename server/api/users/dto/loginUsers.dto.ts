import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsBoolean,
  NotEquals,
  Equals,
  IsDateString,
} from "class-validator";

enum GenderUsers {
  male = "male",
  female = "female",
}

export default class LoginUsersDto {
  @IsEmail()
  email: string;

  @IsString()
  password: string;
}
