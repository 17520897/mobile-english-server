import mongoose from "mongoose";

export interface IUsersModel extends mongoose.Document {
  email: string;
  password: string;
  googleEmail: string;
  name: string;
  avatar: string;
  gender: string;
  dayOfBirth: Date;
  status: string;
  levelInfo: {
    level: number;
    order: number;
  };
  gamesPass: Array<{
    game: string;
    passAt: Date;
  }>;
}

const schema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
    },
    googleEmail: {
      type: String,
    },
    name: {
      type: String,
    },
    avatar: {
      type: String,
    },
    gender: {
      type: String,
      enum: ["male", "female", "other"],
    },
    dayOfBirth: {
      type: Date,
    },
    status: {
      type: String,
      enum: ["normal", "block", "delete"],
      default: "normal",
    },
    levelInfo: {
      level: { type: Number, default: 1 },
      order: {
        type: Number,
        default: 0,
      },
    },
    gamesPass: [
      {
        game: {
          type: mongoose.SchemaTypes.ObjectId,
          ref: "Games",
        },
        passAt: {
          type: Date,
          default: Date.now,
        },
      },
    ],
  },
  {
    collection: "Users",
  }
);

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const Users = mongoose.model<IUsersModel>("Users", schema);
