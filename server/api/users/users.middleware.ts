import { Request, Response, NextFunction } from "express";
import l from "../../common/logger";
import log4j from "../../common/log4js";
import responseService, { HTTPCode } from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import usersService from "./users.service";
import bcrypt from "bcrypt";
import { UsersStatus } from "../../const";

const log = log4j("UsersMiddleware");
class UsersMiddleware {
  async registerUsers(req: Request, res: Response, next: NextFunction) {
    try {
      const { email, password } = req.body;
      const checkEmailExisted = await usersService.getOne({
        $or: [{ email }, { googleEmail: email }],
        status: UsersStatus.normal,
      });
      if (checkEmailExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.usersEmailExisted,
              error: "email existed",
              field: "email",
            },
          ],
        });
      }
      const salt = await bcrypt.genSalt(10);
      req.body.password = await bcrypt.hash(password, salt);
      next();
    } catch (error) {
      l.error(`registerUsers middleware: ${error.message}`);
      log.error(`register: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async loginUsers(req: Request, res: Response, next: NextFunction) {
    try {
      const { email, password } = req.body;
      const checkUsersExisted = await usersService.getOne({
        email,
        status: UsersStatus.normal,
      });
      console.log(checkUsersExisted);
      if (
        !checkUsersExisted ||
        checkUsersExisted.status !== UsersStatus.normal
      ) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.usersNotFound,
              error: "users not found",
              field: "email",
            },
          ],
        });
      }
      const comparePassword = await bcrypt.compare(
        password,
        checkUsersExisted.password
      );
      if (!comparePassword) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.usersWrongPassword,
              error: "users wrong password",
              field: "password",
            },
          ],
        });
      }
      req.body.userId = checkUsersExisted._id;
      next();
    } catch (error) {
      l.error(`loginUsers controller: ${error.message}`);
      log.error(`login: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async updateUsersInfo(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId } = req.cookies;
      const { password, oldPassword } = req.body;
      if (password && oldPassword) {
        const users = await usersService.getOne({
          _id: userId,
          status: UsersStatus.normal,
        });
        if (!users) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.usersNotFound,
                error: "users not found",
                field: "userId",
              },
            ],
          });
        }
        const compare = await bcrypt.compare(oldPassword, users.password);
        if (!compare) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.usersWrongPassword,
                field: "oldPassword",
                error: "wrong old password",
              },
            ],
          });
        }
        const salt = await bcrypt.genSalt(10);
        req.body.password = await bcrypt.hash(password, salt);
      } else {
        req.body.password = undefined;
      }
      next();
    } catch (error) {
      l.error(`updateUsersInfo middleware: ${error.message}`);
      log.error(`updateUserInfo: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new UsersMiddleware();
