import { Request, Response } from "express";
import l from "../../common/logger";
import log4j from "../../common/log4js";
import responseService, { HTTPCode } from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import usersService from "./users.service";
import bcrypt from "bcrypt";
import jwt from "../../services/jwt";
import { UsersStatus } from "../../const";
import mongoose from "mongoose";
import usersHistoryService from "../usersHistory/usersHistory.service";
const log = log4j("UsersController");
class UsersController {
  async registerUsers(req: Request, res: Response) {
    try {
      const users = await usersService.create(req.body);
      users.password = undefined;
      return responseService.send(res, HTTPCode.success, {
        message: "create users success",
        data: users,
      });
    } catch (error) {
      l.error(`registerUsers controller: ${error.message}`);
      log.error(`register: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async registerGoogleUsers(req: Request, res: Response) {
    try {
      const users = await usersService.create(req.body);
      users.password = undefined;
      return responseService.send(res, HTTPCode.success, {
        message: "create users with register google success",
        data: users,
      });
    } catch (error) {
      l.error(`registerGoogleUsers controller: ${error.message}`);
      log.error(`registerGoogle: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async loginUsers(req: Request, res: Response) {
    try {
      const { userId } = req.body;
      const accessToken = await jwt.generateAccessToken({
        userId,
        isUser: true,
      });
      const refreshToken = await jwt.generateRefreshToken({
        userId,
        isUser: true,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "login success",
        data: {
          accessToken,
          refreshToken,
        },
      });
    } catch (error) {
      l.error(`loginUsers controller: ${error.message}`);
      log.error(`login: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async getUsersInfo(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const users = await usersService.getById(userId);
      users.password = undefined;
      return responseService.send(res, HTTPCode.success, {
        message: "get users info success",
        data: users,
      });
    } catch (error) {
      l.error(`getUsers controller: ${error.message}`);
      log.error(`getUsers: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async updateUsersInfo(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const users = await usersService.updateById(userId, req.body);
      users.password = undefined;
      return responseService.send(res, HTTPCode.success, {
        data: users,
        message: "update users info success",
      });
    } catch (error) {
      l.error(`updateUsersInfo controller: ${error.message}`);
      log.error(`updateUsersInfo: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  //admin
  async adminGetListUsers(req: Request, res: Response) {
    try {
      const { page, limit, sortProperty, sortType } = req.query as any;
      const sort = {};
      if (sortType && sortProperty)
        sort[sortProperty] = sortType === "ascending" ? 1 : -1;
      const users = await usersService.getByQuery({
        query: {
          status: UsersStatus.normal,
        },
        page,
        limit,
        sort,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "admin get list users success",
        data: users,
      });
    } catch (error) {
      l.error(`adminGetListUsers controller: ${error.message}`);
      log.error(`adminGetList: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async adminCountUsers(req: Request, res: Response) {
    try {
      const { beginAt, endAt } = req.query;
    } catch (error) {
      l.error(`adminCountUsers: ${error.message}`);
      log.error(`adminCount: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new UsersController();
