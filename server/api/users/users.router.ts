import express from "express";
import { Valid, RequestType } from "../../middlewares/Valid";
import CreateUsersDto from "./dto/createUsers.dto";
import usersMiddleware from "./users.middleware";
import usersController from "./users.controller";
import LoginUsersDto from "./dto/loginUsers.dto";
import UpdateUsersDto from "./dto/updateUsers.dto";
import { validateAccount } from "../../middlewares/validate";
import { AccountType } from "../../const";

const usersRouter = express.Router();

usersRouter.post(
  "/register",
  Valid(CreateUsersDto, RequestType.body),
  usersMiddleware.registerUsers,
  usersController.registerUsers
);

usersRouter.post(
  "/login",
  Valid(LoginUsersDto, RequestType.body),
  usersMiddleware.loginUsers,
  usersController.loginUsers
);

usersRouter.get(
  "/",
  validateAccount(AccountType.users),
  usersController.getUsersInfo
);

usersRouter.put(
  "/",
  Valid(UpdateUsersDto, RequestType.body),
  validateAccount(AccountType.users),
  usersMiddleware.updateUsersInfo,
  usersController.updateUsersInfo
);
export default usersRouter;
