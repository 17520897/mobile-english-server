import express from "express";
import adminRoute from "./admin/admin.router";
import vocabularyRouter from "./vocabulary/vocabulary.router";
import vocabularyPackagesRouter from "./vocabularyPackages/vocabularyPackages.router";
import usersRouter from "./users/users.router";
import crawlRouter from "./crawl/crawl.router";
import gamesRouter from "./games/games.router";
import usersHistoryRouter from "./usersHistory/usersHistory.router";
import testsRouter from "./tests/tests.router";
import uploadFilesRouter from "./uploadFiles/uploadFiles.router";
import crawlHistoryRouter from "./crawlHistory/crawlHistory.router";
const router = express.Router();

router.use("/admin", adminRoute);
router.get("/test", async (req, res) => {
  res.send("test 3");
});

router.use("/vocabulary", vocabularyRouter);
router.use("/vocabularyPackages", vocabularyPackagesRouter);
router.use("/users", usersRouter);
router.use("/crawl", crawlRouter);
router.use("/games", gamesRouter);
router.use("/usersHistory", usersHistoryRouter);
router.use("/gameTests", testsRouter);
router.use("/uploadFiles", uploadFilesRouter);
router.use("/crawlHistory", crawlHistoryRouter);
export default router;
