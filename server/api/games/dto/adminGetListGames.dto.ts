import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsBoolean,
  NotEquals,
  Equals,
  IsDateString,
  IsArray,
  IsNumberString,
} from "class-validator";

export default class AdminGetListGamesDto {
  @IsNumberString()
  page: string;

  @IsNumberString()
  limit: String;

  @IsOptional()
  @IsNumberString()
  level: string;

  @IsOptional()
  @IsMongoId()
  id: string;
}
