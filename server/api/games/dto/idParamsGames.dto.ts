import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsBoolean,
  NotEquals,
  Equals,
  IsDateString,
  IsArray,
} from "class-validator";

export default class IdParamsGameDto {
  @IsMongoId()
  id: string;
}
