import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsBoolean,
  NotEquals,
  Equals,
  IsDateString,
  IsArray,
  IsNumberString,
} from "class-validator";

export default class UsersGetListGamesDto {
  @IsNumberString()
  page: string;

  @IsNumberString()
  limit: String;

  @IsOptional()
  @IsNumberString()
  level: string;
}
