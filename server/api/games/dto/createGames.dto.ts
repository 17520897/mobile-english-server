import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsBoolean,
  NotEquals,
  Equals,
  IsDateString,
  IsArray,
} from "class-validator";

enum GamesQuestionTypes {
  pictures = "pictures",
  listeningVietnamese = "listeningVietnamese",
  listeningEnglish = "listeningEnglish",
  sentence = "sentence",
}

export default class CreateGamestDto {
  @IsString()
  name: string;

  @IsNumber()
  @Min(1)
  level: number;

  @IsNumber()
  order: number;

  @IsOptional()
  @IsNumber()
  requireOrder: number;

  @IsOptional()
  @IsString()
  icon: String;

  @IsOptional()
  @IsArray()
  questions: any;
}
