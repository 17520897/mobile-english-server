import express from "express";
import gamesMiddleware from "./games.middleware";
import gamesController from "./games.controller";
import { Valid, RequestType } from "../../middlewares/Valid";
import CreateGamestDto from "./dto/createGames.dto";
import IdParamsGameDto from "./dto/idParamsGames.dto";
import { validateAccount } from "../../middlewares/validate";
import { AccountType } from "../../const";
import AdminGetListGamesDto from "./dto/adminGetListGames.dto";
import UsersGetListGamesDto from "./dto/usersGetListGames.dto";

const gamesRouter = express.Router();

gamesRouter.post(
  "/",
  Valid(CreateGamestDto, RequestType.body),
  validateAccount(AccountType.admins),
  gamesMiddleware.adminCreateGames,
  gamesController.adminCreateGames
);

gamesRouter.get(
  "/list",
  Valid(AdminGetListGamesDto, RequestType.query),
  validateAccount(AccountType.admins),
  gamesMiddleware.adminGetListGames,
  gamesController.adminGetListGames
);

gamesRouter.put(
  "/:id",
  Valid(IdParamsGameDto, RequestType.params),
  validateAccount(AccountType.admins),
  gamesMiddleware.adminUpdateGames,
  gamesController.adminUpdateGames
);

gamesRouter.delete(
  "/:id",
  Valid(IdParamsGameDto, RequestType.params),
  validateAccount(AccountType.admins),
  gamesMiddleware.adminDeleteGames,
  gamesController.adminDeleteGames
);

gamesRouter.get(
  "/",
  Valid(UsersGetListGamesDto, RequestType.query),
  validateAccount(AccountType.users),
  gamesController.usersGetList
);

gamesRouter.get(
  "/:id",
  Valid(IdParamsGameDto, RequestType.params),
  validateAccount(AccountType.users),
  gamesMiddleware.usersGetLearningGames,
  gamesController.usersGetLearningGames
);

export default gamesRouter;
