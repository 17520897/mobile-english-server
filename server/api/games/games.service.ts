import { Games, IGamesModel } from "./games.model";
import { QueryFindOneAndRemoveOptions, SaveOptions } from "mongoose";

interface IGamesQuery {
  query?: any;
  page?: any;
  limit?: any;
  sort?: any;
  options?: any;
}

interface IGamesPopulate {
  query?: any;
  populate?: Array<{
    match?: any;
    path: string;
    select?: string;
    options?: any;
  }>;
  sort?: any;
  select?: string;
  page?: any;
  limit?: any;
}

export class GamesService {
  async create(data: any, options: SaveOptions = {}): Promise<IGamesModel> {
    const newDoc = new Games(data);
    const doc = await newDoc.save(options);
    return doc;
  }

  async getById(id: any): Promise<IGamesModel> {
    const doc = (await Games.findById(id).lean()) as IGamesModel;
    return doc;
  }

  async getOne(query: any): Promise<IGamesModel> {
    const doc = (await Games.findOne(query).lean()) as IGamesModel;
    return doc;
  }

  async getSort(query: any, sort: any = {}): Promise<any[]> {
    const doc = await Games.find(query).sort(sort);
    return doc;
  }

  async getByQuery(iQuery: IGamesQuery): Promise<any[]> {
    let { page, limit, options, sort, query } = iQuery;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await Games.find(query, options)
        .sort(sort)
        .skip(page * limit)
        .limit(limit);
      const count = await Games.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await Games.find(query, options).sort(sort);
      return doc;
    }
  }

  async deleteOne(query: any): Promise<any> {
    const doc = await Games.findOneAndDelete(query);
    return doc;
  }

  async deleteById(
    id: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Games.findByIdAndDelete(id, options);
    return doc;
  }

  async deleteMany(
    query: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Games.deleteMany(query, options);
    return doc;
  }

  async populate(iPopulate: IGamesPopulate): Promise<any[]> {
    const { query, sort, select, populate } = iPopulate;
    let { page, limit } = iPopulate;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await Games.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .skip(page * limit)
        .limit(limit)
        .lean()
        .exec();
      const count = await Games.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await Games.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .lean()
        .exec();
      return doc;
    }
  }

  async updateOne(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<IGamesModel> {
    const doc = await Games.findOneAndUpdate(query, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateById(
    id,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<IGamesModel> {
    const doc = await Games.findByIdAndUpdate(id, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateMany(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await Games.updateMany(query, data, options);
    return doc;
  }

  async countDocument(query: any = {}) {
    const count = await Games.find(query).countDocuments();
    return count;
  }
}

export default new GamesService();
