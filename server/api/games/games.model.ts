import mongoose from "mongoose";

export interface IGamesModel extends mongoose.Document {
  name?: string;
  level?: number;
  order?: number;
  requireOrder?: number;
  questions?: Array<{
    link: string;
    image: string;
    type?: string;
    question?: string;
    answers?: Array<{
      answer?: string;
      image?: string;
    }>;
    rightAnswer?: string;
  }>;
  testId: string;
  createAt: Date;
  icon: string;
}

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    level: {
      type: Number,
      required: true,
      default: 1,
    },
    icon: {
      type: String,
      default:
        "https://cdn3.iconfinder.com/data/icons/fantasy-and-role-play-game-adventure-quest/512/Dragon_Egg-512.png",
    },
    order: {
      type: Number,
      default: 0,
    },
    requireOrder: {
      type: Number,
      default: 0,
    },
    questions: [
      {
        type: {
          type: String,
          enum: ["pictures", "listening", "sentence"],
        },
        question: {
          // hello my name
          type: String,
        },
        image: {
          type: String,
        },
        link: {
          type: String,
        },
        answers: [
          {
            answer: {
              type: String,
            },
            image: {
              type: String,
            },
            audio: {
              type: String,
            },
          },
        ],
        rightAnswer: {
          type: String,
        },
      },
    ],
    testId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    creatAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: "Games",
  }
);

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const Games = mongoose.model<IGamesModel>("Games", schema);
