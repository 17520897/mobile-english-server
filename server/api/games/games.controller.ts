import e, { Request, Response } from "express";
import l from "../../common/logger";
import log4j from "../../common/log4js";
import responseService, { HTTPCode } from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import gamesService from "./games.service";
import mongoose, { mongo } from "mongoose";
import testsService from "../tests/tests.service";
import usersService from "../users/users.service";
import vocabularyService from "../vocabulary/vocabulary.service";
const log = log4j("GamesController");
class GamesController {
  async adminCreateGames(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      let { level, questions, icon } = req.body;
      let testId = undefined;
      const { updateType } = req.query;
      const checkTestsLevel = await testsService.getOne({
        level: parseInt(level),
      });
      if (!checkTestsLevel) {
        const tests = await testsService.create(
          {
            name: `Test level ${level}`,
            level,
          },
          options
        );
        testId = tests._id;
      } else testId = checkTestsLevel._id;
      if (questions) {
        for (let i = 0; i < questions.length; i++) {
          if (questions[i].type !== "sentence") {
            const rightAnswer = await vocabularyService.getById(
              questions[i].rightAnswer
            );
            questions[i].rightAnswer = rightAnswer.english;
          }
          const answers = [...questions[i].answers];
          questions[i].answers = [];
          for (let j = 0; j < answers.length; j++) {
            const answer = await vocabularyService.getById(answers[j]);
            questions[i].answers.push({
              answer: answer.english,
              image: answer.image,
              audio: answer.audio,
            });
          }
        }
      }
      questions = questions.slice(0, questions.length / 2);
      await testsService.updateOne(
        {
          level,
        },
        {
          $push: {
            questions,
          },
        },
        options
      );
      req.body.questions = questions;
      if (icon) {
        const iconSplit = icon.split("/");
        if (iconSplit[1] === "public") {
          req.body.icon = `http://staging.pd-mobile-web.tk${icon}`;
        }
      }
      const games = await gamesService.create({ ...req.body, testId }, options);
      await session.commitTransaction();
      return responseService.send(res, HTTPCode.success, {
        message: "create games success",
        data: games,
      });
    } catch (error) {
      l.error(`createGames controller: ${error.message}`);
      log.error(`create: `, error);
      await session.abortTransaction();
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    } finally {
      session.endSession();
    }
  }

  async adminGetListGames(req: Request, res: Response) {
    try {
      const { page, limit, level, id } = req.query as any;
      let query = {};
      if (level) query["level"] = parseInt(level);
      if (id) query = { _id: id };
      const games = await gamesService.getByQuery({
        query,
        page,
        limit,
        sort: {
          level: 1,
          order: 1,
          createAt: -1,
        },
      });
      return responseService.send(res, HTTPCode.success, {
        message: "get games success",
        data: games,
      });
    } catch (error) {
      l.error(`getGames controller: ${error.message}`);
      log.error(`get: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async adminUpdateGames(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { id } = req.params;
      const { level, questions, icon } = req.body;
      let testId = undefined;
      if (level) {
        const [checkTestLevelExists, checkNewLevel] = await Promise.all([
          testsService.getOne({ level: parseInt(level) }),
          gamesService.getOne({ _id: id, level: parseInt(level) }),
        ]);
        if (!checkNewLevel && !checkTestLevelExists) {
          const test = await testsService.create(
            {
              level: parseInt(level),
            },
            options
          );
          testId = test._id;
        } else {
          testId = checkTestLevelExists._id;
        }
      }
      if (questions) {
        for (let i = 0; i < questions.length; i++) {
          if (questions[i].type !== "sentence") {
            const rightAnswer = await vocabularyService.getById(
              questions[i].rightAnswer
            );
            questions[i].rightAnswer = rightAnswer.english;
          }
          const answers = [...questions[i].answers];
          questions[i].answers = [];
          for (let j = 0; j < answers.length; j++) {
            const answer = await vocabularyService.getById(answers[j]);
            questions[i].answers.push({
              answer: answer.english,
              image: answer.image,
              audio: answer.audio,
            });
          }
        }
        req.body.questions = questions;
      }
      if (icon) {
        const iconSplit = icon.split("/");
        if (iconSplit[1] === "public") {
          req.body.icon = `http://staging.pd-mobile-web.tk${icon}`;
        }
      }
      const games = await gamesService.updateById(
        id,
        { ...req.body, testId },
        options
      );
      await session.commitTransaction();
      return responseService.send(res, HTTPCode.success, {
        message: "update games success",
        data: games,
      });
    } catch (error) {
      l.error(`updateGames controller: ${error.message}`);
      log.error(`update:  `, error);
      await session.abortTransaction();
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    } finally {
      session.endSession();
    }
  }

  async adminDeleteGames(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { id } = req.params;
      const { level } = req.body;
      const checkAnotherExistedGame = await gamesService.getByQuery({
        query: {
          level: parseInt(level),
          _id: { $ne: id },
        },
      });
      if (checkAnotherExistedGame.length === 0) {
        await testsService.deleteOne({ level: parseInt(level) }, options);
      }
      await gamesService.deleteById(id, options);
      await session.commitTransaction();
      return responseService.send(res, HTTPCode.success, {
        message: "delete games success",
      });
    } catch (error) {
      l.error(`deleteGames controller: ${error.message}`);
      log.error(`delete: `, error);
      await session.abortTransaction();
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    } finally {
      session.endSession();
    }
  }

  async usersGetList(req: Request, res: Response) {
    try {
      let query = {} as any;
      const { page, limit, level, maxLevel } = req.query as any;
      const { userId } = req.cookies;
      if (level) query["level"] = parseInt(level);
      const returnData = {};
      for (let i = 0; i < maxLevel; i++) {
        let games = await gamesService.getByQuery({
          query: {
            level: i + 1,
          },
          page,
          limit,
          sort: {
            level: 1,
            order: 1,
            createAt: -1,
          },
        });
        games.pop();
        const users = await usersService.getById(userId);
        for (let i = 0; i < games.length; i++) {
          if (games[i]._id) {
            games[i] = {
              ...games[i].toObject(),
              isLearn: false,
            };
            for (let j = 0; j < users.gamesPass.length; j++) {
              if (
                games[i]._id.toString() === users.gamesPass[j].game.toString()
              ) {
                games[i]["isLearn"] = true;
              }
            }
          }
        }
        const formatGameData = {};
        for (let i = 0; i < games.length; i++) {
          if (!formatGameData[games[i].order]) {
            formatGameData[games[i].order] = [];
          }
          formatGameData[games[i].order].push(games[i]);
        }
        returnData[i + 1] = formatGameData;
      }

      return responseService.send(res, HTTPCode.success, {
        data: returnData,
        message: "users get list games success",
      });
    } catch (error) {
      l.error(`usersGetList controller: ${error.message}`);
      log.error(`userGetList: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async usersGetLearningGames(req: Request, res: Response) {
    try {
      return responseService.send(res, HTTPCode.success, {
        data: req.body.games,
        message: "user get learning games success",
      });
    } catch (error) {
      l.error(`userGetLearningGames controller: ${error.message}`);
      log.error(`userGetLearningGames: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new GamesController();
