import { Request, Response, NextFunction } from "express";
import l from "../../common/logger";
import log4j from "../../common/log4js";
import responseService, { HTTPCode } from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import gamesService from "./games.service";
import usersService from "../users/users.service";

const log = log4j("GamesMiddleware");
class GamesMiddleware {
  async adminCreateGames(req: Request, res: Response, next: NextFunction) {
    try {
      const { name, requireOrder, level } = req.body;
      const [checkNames, checkRequireOrder] = await Promise.all([
        await gamesService.getOne({ name, level }),
        await gamesService.getOne({
          order: requireOrder ? parseInt(requireOrder) : -1,
          level,
        }),
      ]);
      if (name && checkNames) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.gamesNameExisted,
              error: "names existed",
              field: "name",
            },
          ],
        });
      }
      if (requireOrder && !checkRequireOrder) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.gamesRequireOrderNotExisted,
              error: "order for this require not existed",
              field: "requireOrder",
            },
          ],
        });
      }
      next();
    } catch (error) {
      l.error(`createGames middleware: ${error.message}`);
      log.error(`create: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async adminGetListGames(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.query;
      if (id) {
        const games = await gamesService.getById(id);
        if (!games) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.gamesNotFound,
                error: "games not found",
                field: "id",
              },
            ],
          });
        }
      }
      next();
    } catch (error) {
      l.error(`getGames controller: ${error.message}`);
      log.error(`get: `, error.message);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async adminDeleteGames(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const checkDelete = await gamesService.getById(id);
      if (!checkDelete) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.gamesNotFound,
              field: "id",
              error: "game not found",
            },
          ],
        });
      }
      req.body.level = checkDelete.level;
      next();
    } catch (error) {
      l.error(`deleteGames middleware: ${error}`);
      log.error(`delete: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async adminUpdateGames(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { name, requireOrder, level } = req.body;
      const [games, checkNames, checkRequireOrder] = await Promise.all([
        gamesService.getById(id),
        gamesService.getOne({ name, level, _id: { $ne: id } }),
        gamesService.getOne({
          order: requireOrder ? parseInt(requireOrder) : -1,
          level,
        }),
      ]);
      if (!games) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.gamesNotFound,
              error: "games not found",
              field: "id",
            },
          ],
        });
      }
      if (name && checkNames) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.gamesNameExisted,
              error: "names existed",
              field: "name",
            },
          ],
        });
      }
      if (requireOrder && !checkRequireOrder) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.gamesRequireOrderNotExisted,
              error: "order for this require not existed",
              field: "requireOrder",
            },
          ],
        });
      }
      next();
    } catch (error) {
      l.error(`adminUpdateGames middleware: ${error.message}`);
      log.error(`adminUpdateGames: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
          },
        ],
      });
    }
  }

  async usersGetLearningGames(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { userId } = req.cookies;
      console.log(userId);
      const [checkExistGames, userInfo] = await Promise.all([
        gamesService.getById(id),
        usersService.getById(userId),
      ]);
      if (!checkExistGames) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.gamesNotFound,
              error: "games not found",
              field: "id",
            },
          ],
        });
      }
      const { level, requireOrder } = checkExistGames;
      const { levelInfo } = userInfo;
      if (
        level > levelInfo.level ||
        (level === levelInfo.level && requireOrder > levelInfo.order)
      ) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.gamesUsersNotAcceptLearning,
              error: "user level not accept for learning",
              field: "level",
            },
          ],
        });
      }
      req.body.games = checkExistGames;
      next();
    } catch (error) {
      l.error(`userGetLearningGames middleware: ${error.message}`);
      log.error(`userGetLearning: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new GamesMiddleware();
