import mongoose from "mongoose";

export interface ICrawlHistoryModel extends mongoose.Document {
  text: string;
  url: string;
}

const schema = new mongoose.Schema(
  {
    text: {
      type: String,
      required: true,
    },
    url: {
      type: String,
      required: true,
    },
  },
  {
    collection: "CrawlHistory",
  }
);

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const CrawlHistory = mongoose.model<ICrawlHistoryModel>(
  "CrawlHistory",
  schema
);
