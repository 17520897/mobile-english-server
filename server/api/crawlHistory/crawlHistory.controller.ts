import { Request, Response } from "express";
import log4j from "../../common/log4js";
import l from "../../common/logger";
import { ErrorsCode } from "../../const/errorsCode";
import responseService, { HTTPCode } from "../../services/responseService";
import crawlHistoryService from "./crawlHistory.service";

const log = log4j("CrawlHistoryController");
class CrawlHistoryController {
  async searchCrawlHistory(req: Request, res: Response) {
    try {
      const { text, page, limit } = req.query;
      const crawlHistory = await crawlHistoryService.getByQuery({
        query: {
          $or: [
            {
              text: { $regex: text, $options: "i" },
            },
            {
              url: { $regex: text, $options: "i" },
            },
          ],
        },
        page,
        limit,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "search crawl history success",
        data: crawlHistory,
      });
    } catch (error) {
      l.error(`searchCrawlHistory controller: ${error.message}`);
      log.error(`search: `, error);
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new CrawlHistoryController();
