import { ICrawlHistoryModel, CrawlHistory } from "./crawlHistory.model";
import { QueryFindOneAndRemoveOptions } from "mongoose";

interface ICrawlHistoryQuery {
  query?: any;
  page?: any;
  limit?: any;
  sort?: any;
  options?: any;
}

interface ICrawlHistoryPopulate {
  query?: any;
  populate?: Array<{
    match?: any;
    path: string;
    select?: string;
    options?: any;
  }>;
  sort?: any;
  select?: string;
  page?: any;
  limit?: any;
}

export class CrawlHistoryService {
  async create(data: any): Promise<ICrawlHistoryModel> {
    const newDoc = new CrawlHistory(data);
    const doc = await newDoc.save();
    return doc;
  }

  async createOrUpdate(query: any, data: any): Promise<ICrawlHistoryModel> {
    const doc = await CrawlHistory.findOneAndUpdate(query, data, {
      upsert: true,
      new: true,
    });
    return doc;
  }

  async getById(id: any): Promise<ICrawlHistoryModel> {
    const doc = (await CrawlHistory.findById(id).lean()) as ICrawlHistoryModel;
    return doc;
  }

  async getOne(query: any): Promise<ICrawlHistoryModel> {
    const doc = (await CrawlHistory.findOne(
      query
    ).lean()) as ICrawlHistoryModel;
    return doc;
  }

  async getSort(query: any, sort: any = {}): Promise<any[]> {
    const doc = await CrawlHistory.find(query).sort(sort);
    return doc;
  }

  async getByQuery(iQuery: ICrawlHistoryQuery): Promise<any[]> {
    let { page, limit, options, sort, query } = iQuery;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await CrawlHistory.find(query, options)
        .sort(sort)
        .skip(page * limit)
        .limit(limit);
      const count = await CrawlHistory.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await CrawlHistory.find(query, options).sort(sort);
      return doc;
    }
  }

  async deleteOne(query: any): Promise<any> {
    const doc = await CrawlHistory.findOneAndDelete(query);
    return doc;
  }

  async deleteById(
    id: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await CrawlHistory.findByIdAndDelete(id, options);
    return doc;
  }

  async deleteMany(
    query: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await CrawlHistory.deleteMany(query, options);
    return doc;
  }

  async populate(iPopulate: ICrawlHistoryPopulate): Promise<any[]> {
    const { query, sort, select, populate } = iPopulate;
    let { page, limit } = iPopulate;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await CrawlHistory.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .skip(page * limit)
        .limit(limit)
        .lean()
        .exec();
      const count = await CrawlHistory.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await CrawlHistory.find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .lean()
        .exec();
      return doc;
    }
  }

  async updateOne(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<ICrawlHistoryModel> {
    const doc = await CrawlHistory.findOneAndUpdate(query, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateById(
    id,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<ICrawlHistoryModel> {
    const doc = await CrawlHistory.findByIdAndUpdate(id, data, {
      ...options,
      new: true,
    });
    return doc;
  }

  async updateMany(
    query,
    data,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<any> {
    const doc = await CrawlHistory.updateMany(query, data, options);
    return doc;
  }
}

export default new CrawlHistoryService();
