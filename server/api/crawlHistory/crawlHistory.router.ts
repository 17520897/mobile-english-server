import express from "express";
import crawlHistoryController from "./crawlHistory.controller";

const crawlHistoryRouter = express.Router();

crawlHistoryRouter.get("/search", crawlHistoryController.searchCrawlHistory);

export default crawlHistoryRouter;
