import l from "../common/logger";
import ResponseService, { HTTPCode } from "../services/responseService";
import jsonWebToken from "jsonwebtoken";
import { ErrorsCode } from "../const/errorsCode";
import { Request, Response, NextFunction } from "express";
import responseService from "../services/responseService";
import usersService from "../api/users/users.service";
import log4j from "../common/log4js";
import { AccountType } from "../const";

const log = log4j("Validate");

export function validateAccount(accountType?: AccountType) {
  return async function (req, res, next) {
    try {
      if (!req.headers["authorization"])
        return ResponseService.send(res, HTTPCode.unauthorize, {
          message: "you are not authorize",
        });
      const headersToken = req.headers["authorization"].split(" ");

      if (headersToken[0] !== "Bearer")
        return ResponseService.send(res, HTTPCode.unauthorize, {
          message: "you are not authorize",
        });

      if (!headersToken[1]) {
        return ResponseService.send(res, HTTPCode.unauthorize, {
          message: "you are not authorize",
        });
      }
      const token = jsonWebToken.verify(
        headersToken[1],
        process.env.JWT_SECRET
      ) as any;
      if (!token)
        return ResponseService.send(res, HTTPCode.unauthorize, {
          message: "you are not authorize",
        });
      if (token.isAccess) {
        if (accountType === AccountType.users) {
          if (token.isUser) {
            req.cookies.userId = token.userId;
            next();
          } else {
            return ResponseService.send(res, HTTPCode.unauthorize, {
              message: "you are not authorize",
            });
          }
        } else if (accountType === AccountType.admins && token.isAdmin) {
          if (token.isAdmin) {
            req.cookies.adminId = token.adminId;
            next();
          } else {
            return ResponseService.send(res, HTTPCode.unauthorize, {
              message: "you are not authorize",
            });
          }
        } else {
          req.cookies.isUser = token.isUser;
          req.cookies.isAdmin = token.isAdmin;
          req.cookies.userId = token.userId;
          req.cookies.adminId = token.adminId;
          next();
        }
      } else
        return ResponseService.send(res, HTTPCode.unauthorize, {
          message: "you are not authorize",
        });
    } catch (error) {
      l.error(`validate account: ${error.message}`);
      return ResponseService.send(res, HTTPCode.unauthorize, {
        message: "you are not authorize",
      });
    }
  };
}
