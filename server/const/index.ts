export enum VerifyType {
  register = "register",
  forgotPassword = "forgotPassword",
}

export enum UsersStatus {
  normal = "normal",
  block = "block",
  delete = "delete",
}

export enum AccountType {
  users = "users",
  admins = "admins",
}
