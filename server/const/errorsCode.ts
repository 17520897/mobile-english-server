export enum ErrorsCode {
  invalid = "invalid",
  serverError = "serverError",

  //verify
  adminAdminNotFound = "adminAdminNotFound",
  adminWrongPassword = "adminWrongPassword",
  adminUsernameExisted = "adminUsernameExisted",
  adminPhoneExisted = "adminPhoneExisted",
  //vocabularyPackage
  vocabularyPackageLastDataInAncestorMustBeEquaParent = "vocabularyPackageLastDataInAncestorMustBeEquaParent",
  vocabularyPackageAncestorsNotHaveCorrectFormat = "vocabularyPackageAncestorsNotHaveCorrectFormat",
  vocabularyPackageNameExistedInThisPackage = "vocabularyPackageNameExistedInThisPackage",

  //users
  usersEmailExisted = "usersEmailExisted",
  usersNotFound = "usersNotFound",
  usersWrongPassword = "usersWrongPassword",

  //games
  gamesNameExisted = "gamesNameExisted",
  gamesNameOrderExisted = "gamesNameOrderExisted",
  gamesRequireOrderNotExisted = "gamesRequireOrderNotExisted",
  gamesParentNotExist = "gamesParentNotExist",
  gamesNotFound = "gamesNotFound",
  gamesUsersNotAcceptLearning = "gamesUsersNotAcceptLearning",

  //tests
  testsTestNotFound = "testsTestNotFound",

  //usersHistory
  usersHistoryGameIdExisted = "usersHistoryGameIdExisted",
  usersHistoryGameNotFound = "usersHistoryGameNotFound",

  //vocabulary
  vocabularyWordsExisted = "vocabularyWordsExisted",

  //
  uploadFilesOnlyAcceptImageFiles = "uploadFilesOnlyAcceptImageFiles",
}
