import { Application } from "express";
import homepage from "./api/routes";
import fs from "fs";
import swaggerUi from "swagger-ui-express";
import crawlService from "./api/crawl/crawl.service";
import l from "./common/logger";
import vocabularyService from "./api/vocabulary/vocabulary.service";
import vocabularyPackagesService from "./api/vocabularyPackages/vocabularyPackages.service";
import axiosRequestService from "./services/axiosRequestService";
import crawlHistoryService from "./api/crawlHistory/crawlHistory.service";
import gamesService from "./api/games/games.service";
import testsService from "./api/tests/tests.service";
// import * as swaggerDocument from "../swagger/mergeSwagger.json";
import cheerio from "cheerio";
import request from "request";

export default function routes(app: Application): void {
  // app.get("/", async (req, res) => {
  //   try {
  //     let { page, limit } = req.query as any;
  //     let data = fs.readFileSync("public/defaults/jsonData.json", {
  //       encoding: "utf-8",
  //     }) as any;
  //     data = JSON.parse(data);
  //     const renderData = [];
  //     limit = parseInt(limit);
  //     page = parseInt(page) * limit;
  //     for (let i = page; i < page + limit; i++) {
  //       const keys = Object.keys(data[i]);
  //       const icons = await crawlService.crawlIcons(keys[0], 5);
  //       const audio = await crawlService.crawlTextToSpeech(keys[0]);
  //       const translate = await crawlService.crawlTranslate(keys[0]);
  //       const { vietnamese, spell } = translate;
  //       renderData.push({
  //         english: keys[0],
  //         icons,
  //         audio: audio,
  //         vietnamese,
  //         spell,
  //       });
  //     }
  //     res.render("index.ejs", { data: renderData, page: page / limit, limit });
  //   } catch (error) {
  //     l.error(error);
  //   }
  // });

  app.get("/setup", async (req, res) => {
    const packagesName = "Từ về đồ vật";
    let totalCount = 0;
    for (let i = 0; i <= 3; i++) {
      const name = `${packagesName} ${i + 1}`;
      const vocabularyPackage = await vocabularyPackagesService.create({
        name,
        isLeaf: false,
      });
      const parentId = vocabularyPackage._id;
      let count = 0;
      while (count < 5) {
        const vocabularies = await vocabularyService.getByQuery({
          page: totalCount,
          limit: 10,
        });
        const vocabulary = [];
        for (let j = 0; j < vocabularies.length; j++) {
          vocabulary.push({
            vocabulary: vocabularies[j]._id,
          });
        }
        await vocabularyPackagesService.create({
          name: `Bài ${count + 1}`,
          parent: parentId,
          ancestors: [parentId],
          vocabularies: vocabulary,
          isLeaf: true,
        });
        count++;
        totalCount++;
      }
    }
    res.send("done");
  });

  app.get("/setupGames", async (req, res) => {
    try {
      let { level, limit, page, name, requireOrder, order } = req.query as any;
      level = parseInt(level);
      page = parseInt(page);
      requireOrder = parseInt(requireOrder);
      order = parseInt(order);
      const types = ["pictures", "pictures", "listening", "sentence"];
      const vocabularies = await vocabularyService.getByQuery({
        page,
        limit,
      });
      vocabularies.pop();
      const questions = [];
      for (let i = 0; i < limit; i++) {
        const percentType = Math.floor(Math.random() * 100);
        const type =
          percentType >= 0 && percentType < 45
            ? 0
            : percentType >= 45 && percentType < 75
            ? 1
            : 2;
        if (types[type] === "pictures") {
          const mainQuestion = i;
          let rand = Math.floor(Math.random() * limit);
          while (rand === mainQuestion) {
            rand = Math.floor(Math.random() * limit);
          }
          let rand2 = Math.floor(Math.random() * limit);
          while (rand2 === rand || rand2 === mainQuestion) {
            rand2 = Math.floor(Math.random() * limit);
          }
          const question = `Đâu là ${vocabularies[i].vietnamese}?`;
          const rightAnswer = vocabularies[i].english;
          const answers = [];
          answers.push({
            answer: vocabularies[mainQuestion].english,
            image: vocabularies[mainQuestion].image,
            audio: vocabularies[mainQuestion].audio,
          });
          answers.push({
            answer: vocabularies[rand].english,
            image: vocabularies[rand].image,
            audio: vocabularies[rand].audio,
          });
          answers.push({
            answer: vocabularies[rand2].english,
            image: vocabularies[rand2].image,
            audio: vocabularies[rand2].audio,
          });
          answers.sort((a, b) => {
            return a.image > b.image ? 1 : -1;
          });
          questions.push({
            type: "pictures",
            question,
            rightAnswer,
            answers,
          });
        } else if (types[type] === "listening") {
          const mainQuestion = i;
          const question = `Chọn từ bạn đã nghe?`;
          const rand = Math.floor(Math.random() * 4);
          const answers = [];
          const rightAnswer = vocabularies[i].english;
          const link = vocabularies[mainQuestion].audio;
          let answerData = vocabularies.slice(rand, rand + 4) as any;
          answerData.push(vocabularies[mainQuestion]);
          answerData.sort((a, b) => {
            return a.image > b.image ? 1 : -1;
          });
          answerData = new Set(answerData);
          for (let item of answerData) {
            answers.push({
              answer: item.english,
              image: item.image,
              audio: item.audio,
            });
          }
          questions.push({
            type: types[type],
            question,
            link,
            rightAnswer,
            answers,
          });
        } else if (types[type] === "sentence") {
          const mainQuestion = i;
          const question = `Chọn nghĩa của từ bằng tiếng việt?`;
          const rand = Math.floor(Math.random() * 4);
          const answers = [];
          const rightAnswer = vocabularies[i].vietnamese;
          const link = vocabularies[mainQuestion].audio;
          let answerData = vocabularies.slice(rand, rand + 4) as any;
          answerData.push(vocabularies[mainQuestion]);
          answerData.sort((a, b) => {
            return a.image > b.image ? 1 : -1;
          });
          answerData = new Set(answerData);
          for (let item of answerData) {
            answers.push({
              answer: item.vietnamese,
              image: item.image,
              audio: item.audio,
            });
          }
          questions.push({
            type: types[type],
            question,
            link,
            rightAnswer,
            answers,
          });
        }
      }
      axiosRequestService.post("http://localhost:2223/api/games", {
        name,
        level,
        order,
        requireOrder,
        questions,
      });
      res.send("done");
    } catch (error) {
      res.send(error);
    }
  });

  app.get("/setupCrawlHistory", async (req, res) => {
    let { page, limit } = req.query as any;
    let data = fs.readFileSync("public/defaults/jsonData.json", {
      encoding: "utf-8",
    }) as any;
    data = JSON.parse(data);
    let count = 0;
    console.log(data.length);
    for (let i = 0; i < data.length; i++) {
      const keys = Object.keys(data[i]);
      const vocabulary = await vocabularyService.getOne({ english: keys[0] });
      console.log(vocabulary);
      if (vocabulary) {
        await crawlHistoryService.createOrUpdate(
          { text: keys[0] },
          {
            text: vocabulary.english,
            url: vocabulary.audio,
          }
        );
        count++;
        console.log(count);
      }
    }
    console.log("done");
    res.send("done");
  });

  app.get("/setupGamesTest", async (req, res) => {
    try {
      let { level, number, limit } = req.query as any;
      level = parseInt(level);
      number = parseInt(number);
      limit = parseInt(limit);
      const games = await gamesService.getByQuery({
        query: {
          level,
        },
      });
      games.pop();
      let gamesData = [];
      for (let i = 0; i < games.length; i++) {
        for (let j = 0; j < games[i].questions.length; j++) {
          games[i].questions[j]._id = undefined;
          gamesData.push({
            type: games[i].questions[j].type,
            question: games[i].questions[j].question,
            image: games[i].questions[j].image,
            link: games[i].questions[j].link,
            rightAnswer: games[i].questions[j].rightAnswer,
            answers: games[i].questions[j].answers,
          });
        }
      }
      gamesData.sort((a, b) => {
        return a.rightAnswer > b.rightAnswer ? 1 : -1;
      });
      const questions = [];
      for (let i = 0; i < gamesData.length; i++) {
        const index = questions.findIndex(
          (item) =>
            item.type === gamesData[i].type &&
            item.rightAnswer === gamesData[i].rightAnswer
        );
        if (index === -1) {
          questions.push(gamesData[i]);
        }
      }
      await testsService.updateOne(
        { level },
        {
          questions: questions.slice(0, number),
        }
      );
      res.send("oke");
    } catch (error) {
      l.error(error);
    }
  });

  app.use("/api", homepage);
  // if (process.env.NODE_ENV === "staging" || process.env.NODE_ENV === "dev")
  //   app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  app.get("/test", async (req, res) => {
    const { text } = req.query;
    const URL = `https://dictionary.cambridge.org/vi/dictionary/english-vietnamese/${text}`;
    request(URL, function (error, response, body) {
      if (error) {
        return console.error("There was an error!");
      }
      var $ = cheerio.load(body);
      const vietnamese = $(".trans");
      const spell = $(".pron-info");
      const example = $(".eg");

      console.log(vietnamese.contents().first().text());
      console.log(spell.contents().first().text());
      console.log(example.first().text());

      res.send("ok");
    });
  });
}
