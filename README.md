# PADEnglish

## Quick Start

Copy file `.env` vào thư mục gốc của dự án

Thực thi code để lập trình tại dev

```shell
# cài đặt run-rs
npm install -g run-rs

# run db
npm run db

# thay biến môi trường
 - Thay thế biến MONGODB_URI_DEV của file .env đúng với đường dẫn replica-set cho run-rs tạo ra

# run dev
npm run dev
```

---

## Lưu ý:

Tạo các branch từ branch dev để code chức năng

- Đặt tên branch: <Tên người thực hiện>/<Tên chức năng>
- Pull code từ dev trước khi push code
- Tạo merge request khi hoàn thành chức năng
- Sau khi được team review code sẽ merge vào branch dev

Quy tắc đặt tên biến:

- Đặt tên biến và hàm theo dạng camelCase
- Tất cả tên biến và hàm đều dùng tiếng Anh
