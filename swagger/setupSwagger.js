const fs = require("fs");

function main() {
  try {
    const files = ["adminAccount"];

    let tags = [];
    let definitions = {};
    let paths = {};
    for (let i = 0; i < files.length; i++) {
      const swaggerData = require(`./swaggerData/${files[i]}Swagger.json`);
      if (swaggerData) {
        tags = [...tags, ...swaggerData.tags];
        (definitions = { ...definitions, ...swaggerData.definitions }),
          (paths = { ...paths, ...swaggerData.paths });
      }
    }
    console.log(tags);
    console.log(definitions);
    console.log(paths);
    if (process.argv.length <= 2) {
      console.log("must have params: [dev, staging, production]");
      return;
    }
    const env = process.argv[2];
    const host = env === "staging" ? "staging.pawnpd.tk" : "localhost:2222";
    console.log(host);
    fs.writeFileSync(
      "swagger/mergeSwagger.json",
      JSON.stringify({
        swagger: "2.0",
        info: {
          version: "1.0.0",
          title: "Pawn Serve",
          description: "Pawn",
          license: {
            name: "MIT",
            url: "https://opensource.org/licenses/MIT",
          },
          contact: {
            url: "https://codosaholding.com/",
          },
        },
        host,
        basePath: "/api",
        schemes: ["http", "https"],
        consumes: ["application/json"],
        produces: ["application/json"],
        securityDefinitions: {
          employeeAuthorization: {
            type: "apiKey",
            name: "authorization",
            in: "header",
          },
          adminAuthorization: {
            type: "apiKey",
            name: "authorization",
            in: "header",
          },
          shopOwnerAuthorization: {
            type: "apiKey",
            name: "authorization",
            in: "header",
          },
          shopEmployeeAuthorization: {
            type: "apiKey",
            name: "authorization",
            in: "header",
          },
          userAuthorization: {
            type: "apiKey",
            name: "authorization",
            in: "header",
          },
        },
        tags,
        definitions,
        paths,
      }),
      {
        encoding: "utf-8",
      }
    );
    console.log(`merge swagger success`);
  } catch (error) {
    console.log(`merge swagger file error: `, error);
    process.exit(1);
  }
}

main();
