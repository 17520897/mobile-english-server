const REPO = "git@gitlab.com:17520897/mobile-english-server.git";
module.exports = {
  apps: [
    {
      name: "padenglish",
      script: "./dist/index.js",
      env: {
        PORT: 2223,
        NODE_ENV: "staging",
      },
      env_production: {
        PORT: 2223,
        NODE_ENV: "production",
      },
    },
  ],
  deploy: {
    staging: {
      user: "root",
      host: "staging.pd-mobile-web.tk",
      ref: "origin/master",
      repo: REPO,
      ssh_options: "StrictHostKeyChecking=no",
      path: `/root/staging/mobile-english-server`,
      "pre-deploy": "git pull",
      "post-deploy":
        "npm install && npm run setup staging && npm run build" +
        " && pm2 startOrRestart ecosystem.config.js" +
        " && pm2 save",
    },
  },
};
